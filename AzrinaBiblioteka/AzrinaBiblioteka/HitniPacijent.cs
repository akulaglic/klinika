﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    public class HitniPacijent : Pacijent
    {
        public string uzrok, prva_pomoc;
        public DateTime vrijeme_smrti;
        public DateTime datum_obdukcije;
        public bool prezivio;
        public HitniPacijent(Knjizica k, Ordinacija o=null, int br = 0, string v = "", string u = "", string d = "") : base(k, o)
        {

            vrijeme_smrti = new DateTime();
            uzrok = u;
            datum_obdukcije = new DateTime();

        }



    }

}
