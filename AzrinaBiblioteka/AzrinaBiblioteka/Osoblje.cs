﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace AzrinaBiblioteka
{
	[Serializable()]
    public class Osoblje:ISerializable
		
    {
		
		public int plata { get; set; }		
		public string ime { get; set; }		
		public string prezime { get; set; }
		public Ordinacija ordinacija = null;
	
	
		public Osoblje(int pl = 400, string i = "", string p = "", Ordinacija o=null)
        {
            ime = i;
            prezime = p;
            plata = pl;
           if(o == null) ordinacija = new Ordinacija();
        }


		public Osoblje() {}
		
	}

}
