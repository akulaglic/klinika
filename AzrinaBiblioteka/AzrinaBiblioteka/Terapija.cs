﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    public class Terapija
    {
        public string terapija { get; set; }
        public string garancija { get; set; }
        public string datum { get; set; }
        public string misljenje_doktora { get; set; }
        public Terapija(string t = "", string g = "", string d = "", string m = "")
        {
            terapija = t;
            garancija = g;
            datum = d;
            misljenje_doktora = m;
        }

    }
} 