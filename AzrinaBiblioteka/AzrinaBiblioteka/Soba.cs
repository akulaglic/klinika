﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{

    public abstract class Soba
    {
        public int broj_posjeta;
       
        public string ime_ordinacije { get; set; }
        public int broj_ordinacije { get; set; }
        public List<Doktor> doktor { get; set; }
        public List<Pacijent> zakazani_pacijenti { get; set; }
        
        public List<int> red_cekanja { get; set; }



        public Soba(string ime = "", int b = 0)
        {

            ime_ordinacije = ime;
            zakazani_pacijenti = new List<Pacijent>();
            broj_ordinacije = b;
            red_cekanja = new List<int>();
            broj_posjeta = 0;
            doktor = new List<Doktor>();
        }

        public virtual void DodajDoktora(Doktor d)
        {
            if (broj_ordinacije < 8) doktor.Add(d);
        }


    }
    

}