﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    public class Ordinacija : Soba
    {
        public Aparat aparat { get; set; }

        public Ordinacija(string ime = "", int b = 0) : base(ime, b)
        {
            aparat = new Aparat(this);
        }

        public override void DodajDoktora(Doktor d)
        {
            base.DodajDoktora(d);
            broj_ordinacije++;
        }
    }
}
