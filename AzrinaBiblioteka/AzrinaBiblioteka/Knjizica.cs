﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
	[Serializable()]
    public class Knjizica : ISerializable
	{
        public string ime { get; set; }
        public string prezime { get; set; }
        public string jmbg, spol, adresa, bracno_stanje;
        public DateTime datum_rodjenja, datum;
        public Knjizica(string i = "", string p = "", string j = "", string s = "", string a = "", string b = "")
        {
            ime = i; prezime = p; jmbg = j; spol = s; adresa = a; bracno_stanje = b;
            datum = new DateTime();
            datum_rodjenja = new DateTime();
        }
		protected Knjizica(SerializationInfo info, StreamingContext context)

		{
			ime = info.GetString("Ime");
			prezime = info.GetString("Prezime");
			
		}



		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Ime", ime);
			info.AddValue("Prezime", prezime);
			
		}
	}
}
