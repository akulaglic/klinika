﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    public class Aparat
    {
        public string ime_aparata { get; set; }
        public int min_u_kvaru { get; set; }
        public int popravljen { get; set; }
        public bool da_li_je_u_kvaru { get; set; }

        public Aparat(Ordinacija o)
        {
            ime_aparata = "";
            min_u_kvaru = 0;
            popravljen = 1;
            da_li_je_u_kvaru = false;
        }
        public void PopraviAparat(Ordinacija o)
        {
            if (o.ime_ordinacije == "kardioloska") min_u_kvaru = 30;
            else if (o.ime_ordinacije == "hiruska") min_u_kvaru = 45;
            else if (o.ime_ordinacije == "radioloska") min_u_kvaru = 60;
        }

    }
}
