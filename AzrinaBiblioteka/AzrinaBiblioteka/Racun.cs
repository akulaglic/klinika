﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    //Ovdje je klasa Racun partial, zato jer je porilicno duga
    public partial  class Racun
    {
        public bool da_li_je_platio { get; set; }
        Klinika k { get; set; }
        public List<int> lista_rata { get; set; }
        public List<double> lista { get; set; }
        public int koliko_rata { get; set; }
        
        public Racun(Pacijent p)
        {
            lista = new List<double>();
            lista_rata = new List<int>();
            k = new Klinika();
            koliko_rata = 0;
            da_li_je_platio = false;
        }

        public void NapraviRacun(Pacijent p, Ordinacija o)
        {

        }
        public double DajDug(Pacijent p, Ordinacija o)
        {
            if (p.velicina_rate == 0) p.dug=0;
            else p.velicina_rate = p.velicina_rate - koliko_rata;
            double d=0;
            double cijena = PostaviCijene(o.ime_ordinacije);
        
            if (p.posjeta > 2)
            {
                k.br_redovnih++;
                cijena -= cijena * 0.1;
            }

            if (p.rate == true)
            {
                double jedna_rata = (cijena + cijena * 0.15) / 6;
                d= jedna_rata * p.velicina_rate;
            }
            else d= cijena;
            return d;
        }

        public void NaplataPacijenta(Pacijent p, Ordinacija o)
        {
            
            double d=DajDug(p,o);
     
            if (p.rate == true)
            {
                
                lista_rata.Add(p.velicina_rate);
                p.dug -= d ;
              

            }
            else p.dug -= d;
        
        }

       
        public string IspisiInterface(Pacijent p, Doktor doc)
        {
          
            string tekst;
            
            tekst = "Racun pacijenta " + p.knjizica.ime +" "+ p.knjizica.prezime+" je: \n\n" ;

            tekst += doc.ordinacija.ime_ordinacije + " usluga za pacijenta " + p.knjizica.ime + " iznosi " + Convert.ToString(p.racun.PostaviCijene(doc.ordinacija.ime_ordinacije))+ ".\n";
                if( p.dug> 0) tekst += "Pacijent ima " + p.dug.ToString() + " duga na racunu.\n";
                else tekst += "Pacijent nema duga na racunu.\n";
               
           
          
            int ukupno = 0;
             foreach (int r in lista_rata)
             {
                 ukupno += r;
             }
            
           
            if (p.rate == true)
            {
                tekst += "Pacijent je platio " + ukupno.ToString()+ " rata, od preostalih " + (6-ukupno).ToString() + " rata.\n";
                foreach (int l in lista_rata)
                {
                    if (l == 6 - ukupno)
                    {
                        lista_rata.Remove(6 - ukupno);
                        break;
                    }
                 }
            }
           
            return tekst;
        }
    }
}
