﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;

namespace AzrinaBiblioteka
{ 

	[Serializable()]
     public class Pacijent : ISerializable
	{

        private Image slika { get; set; }
        public double dug { get; set; }
        public string alergija { get; set; }
        public string informacije_o_bolestima, zakljucak_doktora, inf_porodica;
        public Racun racun { get; set; }
        public Ordinacija ordinacija { get; set; }
        public bool rate { get; set; }
        public int posjeta { get; set; }
        public DateTime mjesec_slike { get; set; }
        public EKarton karton { get; set; }
        public Knjizica knjizica { get; set; }
        public string zakazano { get; set; } 
        public List<Raspored> lista_rasporeda { get; set; }
        public Raspored raspored { get; set; }
        public int broj_na_redu{get; set;}
		public string terapija { get; set; }
        public int velicina_rate { get; set; }


		protected Pacijent(SerializationInfo info, StreamingContext context)

		{
			knjizica.ime = info.GetString("Ime");
			knjizica.prezime = info.GetString("Prezime");
			
		}



		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Ime", knjizica.ime);
			info.AddValue("Prezime", knjizica.prezime);
			
		}

		public Pacijent(Knjizica k=null, Ordinacija o=null)
        {
            
            zakazano = null;
            posjeta = 0;
            rate = false;
            racun = new Racun(this);
            lista_rasporeda = new List<Raspored>();
            raspored = new Raspored(this);
            karton = new EKarton(this);
            if(o==null) o = new Ordinacija();
			if (k == null) k = new Knjizica();
			velicina_rate = 6;
            knjizica = k; 
            ordinacija = o;
            mjesec_slike = new DateTime();
            broj_na_redu = 1;
            dug = 0;
            terapija = "";
            slika = null;

           
        }
        public virtual string IspisiInterface(Pacijent p)
        {

            return "Ime: " + knjizica.ime + "\nPrezime: " + knjizica.prezime + "\nDatum rodjenja: " + knjizica.datum_rodjenja.ToString() + "\nMaticni broj: " +
                 knjizica.jmbg + "\nSpol: " + knjizica.spol + "\nAdresa: " + knjizica.adresa + "\nBracno stanje: " + knjizica.bracno_stanje +
                 "\nDatum upisa: " + knjizica.datum.ToString();

        }
        public void PostaviSliku(Image i)
        {
            slika = i;
        }
        public Image DajSliku() { return slika; }
    }
}
