﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.Serialization;

namespace AzrinaBiblioteka
{
	[Serializable()]
    public class NormalniPacijent : Pacijent
    {
        private string lozinka { get; set; }
        private string id { get; set; }
        
        public int broj_knjizice { get; set; }


		protected NormalniPacijent(SerializationInfo info, StreamingContext context)

		{
			knjizica.ime = info.GetString("Ime");
			knjizica.prezime = info.GetString("Prezime");
			
		}



		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Ime", knjizica.ime);
			info.AddValue("Prezime", knjizica.prezime);
			
		}



		public NormalniPacijent(Knjizica k, Ordinacija o=null, int br = 0) : base(k, o)
        {
            broj_knjizice = br;
            id = k.ime+k.prezime;
            lozinka = MD5Hash(id);
            
        }
        
        public string DajID()
        {
            return id;
        }
        public string DajLozinku()
        {
            return lozinka;
        }

        public override string IspisiInterface(Pacijent p)
        {
            string t;

            t = "Ime: " + knjizica.ime + "\nPrezime: " + knjizica.prezime + "\nDatum rodjenja: " + knjizica.datum_rodjenja.ToString() + "\nMaticni broj: " +
                 knjizica.jmbg + "\nSpol: " + knjizica.spol + "\nAdresa: " + knjizica.adresa + "\nBracno stanje: " + knjizica.bracno_stanje +
                 "\nDatum upisa: " + knjizica.datum.ToString() + "\nInformacije o ranijim bolestima: " + informacije_o_bolestima + "\nInformacije o porodicnoj bolesti: "
                 + inf_porodica + "\nAlergije: " + alergija + "\nZakljucak doktora: " + zakljucak_doktora + "\n";


            return t;
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

    }
}
