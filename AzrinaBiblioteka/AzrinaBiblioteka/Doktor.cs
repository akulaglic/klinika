﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace AzrinaBiblioteka
{
	[Serializable()]
	public class Doktor : Osoblje
	{
		
		private string lozinka ;
		private string id;
		public DateTime dan;
		public List<Pacijent> za_ljekarsko ;

		public List<EKarton> kartoni ;
		private int broj_kartona;
		private int bonus;
		private Terapija terapija;
		public DateTime datumUnosa ;
		public string ime_ordinacije;

		protected Doktor(SerializationInfo info, StreamingContext context)

		{
			ime = info.GetString("Ime");
			prezime = info.GetString("Prezime");
			plata = info.GetInt32("Plata");
			ime_ordinacije = info.GetString("Ordinacija");
		}
		public Doktor() {
			id = ime + prezime;
			lozinka = MD5Hash(id);

			bonus = 0;
			kartoni = new List<EKarton>();
			broj_kartona = 0;
			plata = 2000;
			terapija = new Terapija();
			za_ljekarsko = new List<Pacijent>();
			dan = new DateTime();
			datumUnosa = new DateTime();
			ordinacija = new Ordinacija();
		}


		public  void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Ime", ime);
			info.AddValue("Prezime", prezime);
			info.AddValue("Plata", plata);
			info.AddValue("Ordinacija", ime_ordinacije);
		}
		public Doktor(int plata = 2000, string ime = "", string prezime = "", Ordinacija o=null, int b = 10) : base(plata, ime, prezime,o)
        {
            id =ime+prezime;
            lozinka = MD5Hash(id);
           
            bonus = b;
            kartoni = new List<EKarton>();
            broj_kartona = 0;
            plata = 2000;
            terapija = new Terapija();
            za_ljekarsko = new List<Pacijent>();
            dan = new DateTime();
            datumUnosa = new DateTime();
			if (o == null) ordinacija = new Ordinacija();
  			
        }

		
		public string DajID()
        {
            return id;
        }
        public string DajLozinku()
        {
            return lozinka;
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public void DodajKarton(EKarton k)
        {
            kartoni.Add(k);
            broj_kartona++;

        }
        public void PregledajPacijenta(EKarton k, Ordinacija o, Racun r)
        {
            kartoni.Add(k);
            k.p.posjeta++;
            if (o.ime_ordinacije == "ljekarsko")

            {
                za_ljekarsko.Add(k.p);
                
                kartoni.Remove(k);
                o.zakazani_pacijenti.Remove(k.p);

                if (broj_kartona <= 20) plata += bonus;
                return;
            }

            
            kartoni.Remove(k);
            o.zakazani_pacijenti.Remove(k.p);
            k.p.raspored.ZavrsioPregled(k.p, o);
            if (broj_kartona <= 20 && dan==DateTime.Today) plata += bonus;

        }

        public string DodatniPregled(Pacijent p, Ordinacija o)
        {
            Klinika k = new Klinika();
            string t = "Napravljen je raspored za drugu ordinaciju.";
            foreach (string or in k.imena)
            {
                if (o.ime_ordinacije != or) t = "Potrebno je otici na drugo mjesto, jer klinika ne vrsi ovaj pregled, te napraviti novi raspored.";

                p.raspored.NapraviRaspored(p, o);
            }
            return t;
        }

    }

}
