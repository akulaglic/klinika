﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.Serialization;

namespace AzrinaBiblioteka
{
	[Serializable()]
    public class MedicinskiTehnicar : Osoblje
    {
        private string lozinka { get; set; }
        private string id { get; set; }
		public string ime_ordinacije;
        
        public DateTime datumUnosa { get; set; }
		public MedicinskiTehnicar(int plata = 700, string ime = "", string prezime = "", Ordinacija o = null) : base(plata, ime, prezime, o)
		{
			plata = 800;
			id = ime + prezime;
			lozinka = MD5Hash(id);
			if (o == null) ordinacija = new Ordinacija();
			datumUnosa = new DateTime();

		}

		public MedicinskiTehnicar() { }

		protected MedicinskiTehnicar(SerializationInfo info, StreamingContext context)

		{
			ime = info.GetString("Ime");
			prezime = info.GetString("Prezime");
			plata = info.GetInt32("Plata");
			ime_ordinacije = info.GetString("Ordinacija");
		}



		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Ime", ime);
			info.AddValue("Prezime", prezime);
			info.AddValue("Plata", plata);
			info.AddValue("Ordinacija", ime_ordinacije);
		}

		public string DajID()
        {
            return id;
        }
        public string DajLozinku()
        {
            return lozinka;
        }

        public void NaruciPacijenta(Klinika k, Pacijent p, Ordinacija o)
        {
            if (p == null || k == null || o == null) throw new NullReferenceException("Nije inicijalizovano");
            p.raspored.NapraviRaspored(p, o);
            p.lista_rasporeda.Add(p.raspored);
            //k.pacijenti.Add(p);
            o.broj_posjeta++;
            o.zakazani_pacijenti.Add(p);
        }

        public void ProslijediDoktoru(Doktor d, EKarton k)
        {
            d.DodajKarton(k);
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

    }
}
