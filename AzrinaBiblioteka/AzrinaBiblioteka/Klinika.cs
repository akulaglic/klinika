﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{

    public interface DajString
    {
        string IspisiInterface(Pacijent p);
    }
    //Klasa Racun je partial, jer je poprilicno duza od ostalih
    public partial class Racun 
    {
        

        public double PostaviCijene(string ime)
        {
            double cijena = 0;
            if (ime == "ortopedska") cijena = 500;
            else if (ime == "stomatoloska") cijena = 1000;
            else if (ime == "otorinolaringoloska") cijena = 600;
            else if (ime == "internisticka") cijena = 500;
            else if (ime == "dermatoloska") cijena = 400;
            else if (ime == "laboratorijska") cijena = 300;
            else if (ime == "oftamoloska") cijena = 600;
            else if (ime == "ljekarsko") cijena = 20;
            else if (ime == "kardioloska") cijena = 100;
            else if (ime == "radioloska") cijena = 150;
            else if (ime == "hiruska") cijena = 300;

            return cijena;
        }
     
       
    }


    public class Klinika
    {
       
        public List<string> imena { get; set; }
        public int br_redovnih { get; set; }
        public int budzet_klinike { get; set; }
        public int broj_osoblja { get; set; }
        public int broj_kartona { get; set; }
        public int broj_ordinacija { get; set; }
        public int broj_pacijenata { get; set; }
        public List<Osoblje> osoblje { get; set; }
        public List<EKarton> kartoni { get; set; }
        public List<Soba> ordinacije { get; set; }
        public List<Pacijent> pacijenti { get; set; }
        public Klinika()
        {

            br_redovnih = 0;
            broj_osoblja = 0; broj_kartona = 0; broj_ordinacija = 0; broj_pacijenata = 0;
            osoblje = new List<Osoblje>();
            kartoni = new List<EKarton>();
            ordinacije = new List<Soba>();
            pacijenti = new List<Pacijent>();
            imena = new List<string>();

            imena.Add("ortopedska"); imena.Add("stomatoloska"); imena.Add("otorinolaringoloska"); imena.Add("internisticka"); imena.Add("dermatoloska");
            imena.Add("laboratorijska"); imena.Add("oftamoloska"); imena.Add("ljekarsko"); imena.Add("kardioloska"); imena.Add("radioloska"); imena.Add("hiruska");
        }
        public string NajposjecenijaOrdinacija()
        {
            int najveci = 0;
            foreach (Ordinacija o in ordinacije) if (o.broj_posjeta > najveci) najveci = o.broj_posjeta;

            foreach (Ordinacija o in ordinacije) if (o.broj_posjeta == najveci) return o.ime_ordinacije;
            return "Nijedna ordinacija nije posjecena do sada";

        }

        public void DodajOrdinaciju(Ordinacija o)
        {
            if ( o == null) throw new NullReferenceException("Nije inicijalizovano");
            ordinacije.Add(o);
            broj_ordinacija++;
        }
        public void DodajOsobu(Osoblje o)
        {
            osoblje.Add(o);
            broj_osoblja++;
        }
        public void DodajKarton(EKarton k)
        {
            kartoni.Add(k);
            broj_kartona++;
        }
        public void ObrisiKarton(EKarton k)
        {
            kartoni.Remove(k);
            broj_kartona--;
        }
        public void ObrisiPacijenta(Pacijent p)
        {

            pacijenti.Remove(p);
            broj_pacijenata--;
            ObrisiKarton(p.karton);
         
        }
        public void RegistrujPacijenta(Pacijent p)
        {
            EKarton k = new EKarton(p);
            pacijenti.Add(p);
            broj_pacijenata++;
            DodajKarton(k);
        }

    }

  


    
   

  

}



