﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzrinaBiblioteka
{
    public sealed class Raspored : DajString
    {
        public bool postoji;
        public DateTime datum { get; set; }
        public List<string> vrijeme { get; set; }
        public int h, min;
        public string v { get; set; }

        public Raspored(Pacijent p)
        {
            postoji = false;
            v = "";
            h = 8;
            min = 0;   
            vrijeme = new List<string>();
            datum = new DateTime();
        }

        public void NapraviRaspored(Pacijent p, Ordinacija o)
        {
            postoji = true;
            try
            {
                if (o.aparat.da_li_je_u_kvaru) PovecajVrijeme(o.aparat.min_u_kvaru);
                else PovecajVrijeme(30);
            }
            catch(Exception)
            {
				
            }
            foreach (int r in o.red_cekanja) p.broj_na_redu++;
            p.ordinacija = o;
            o.red_cekanja.Add(p.broj_na_redu);
            datum = DateTime.Today;
          
        }

        public void ZavrsioPregled(Pacijent p,Ordinacija o)
        {
            o.red_cekanja.Remove(p.broj_na_redu);
        }
        public void PovecajVrijeme(int koliko)
        {
            min += koliko;
            if (min >= 60)
            {
                h++;
                min -= 60;
            }
            if (h == 17) throw new Exception("Kraj radnog vremena!");

            v = h.ToString() + ":" + min.ToString();
            vrijeme.Add(v);
        }

        public string IspisiInterface(Pacijent p)
        {
            try
            {
                PovecajVrijeme(30);
            }catch (Exception)
            {
                
            }
            string t = "Raspored pacijenta " + p.knjizica.ime + "\n";
            t += "Termini su: \n";

            foreach (string s in vrijeme) t += "Dana "+datum+" u "+s + " h, u ordinaciji " + p.ordinacija.ime_ordinacije + "\n";
            return t;
        }
    }

}
