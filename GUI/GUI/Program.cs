﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.IO;

namespace GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Klinika k17732 = new Klinika();
			Directory.CreateDirectory("Osoblje");
			Directory.CreateDirectory("Izuzeci");
			Directory.CreateDirectory("Pacijenti");
			Ordinacija o = new Ordinacija("radioloska");
            Ordinacija o1 = new Ordinacija("kardioloska");
            Ordinacija o2 = new Ordinacija("oftamoloska");
            Ordinacija o3 = new Ordinacija("stomatoloska");
            Ordinacija o4 = new Ordinacija("hiruska");
            Ordinacija o5 = new Ordinacija("dermatoloska");
            Ordinacija o6 = new Ordinacija("otorinolaringoloska");
            Ordinacija o7 = new Ordinacija("laboratorijska");
            Ordinacija o8 = new Ordinacija("ljekarsko");
            Ordinacija o9 = new Ordinacija("ortopedska");
            k17732.DodajOrdinaciju(o); k17732.DodajOrdinaciju(o1);
            k17732.DodajOrdinaciju(o2); k17732.DodajOrdinaciju(o3);
            k17732.DodajOrdinaciju(o4); k17732.DodajOrdinaciju(o5);
            k17732.DodajOrdinaciju(o6); k17732.DodajOrdinaciju(o7);
            k17732.DodajOrdinaciju(o8); k17732.DodajOrdinaciju(o9);
         
            Form1 f1 = new Form1(k17732);
             Form3 f3 = new Form3(k17732);
             Form2 f2 = new Form2(k17732);
             Form5 f5 = new Form5(k17732);
             Form7 f7 = new Form7(k17732);
             Form4 f4 = new Form4(k17732);
            Form6 f6 = new Form6(k17732);
            Form8 f8 = new Form8(k17732);
            //Ovdje sam uzela za primjer kako radi grafik neke konkretne vrijednosti
            o1.broj_posjeta = 2;
            o2.broj_posjeta = 5;
            o3.broj_posjeta = 10;
            o4.broj_posjeta = 12;
			o5.broj_posjeta = 20;
			o6.broj_posjeta = 3;
			o7.broj_posjeta = 5;
			Knjizica knj1 = new Knjizica("Ana", "Marija","1111111111111");
            NormalniPacijent p1 = new NormalniPacijent(knj1);
            Knjizica knj4 = new Knjizica("Irma", "Sabic", "1111111111111");
            NormalniPacijent p4 = new NormalniPacijent(knj4);
            Knjizica knj2 = new Knjizica("Sanja", "Peric","2222222222222");
            NormalniPacijent p2 = new NormalniPacijent(knj2);
            Knjizica knj3 = new Knjizica("Sara", "Kucic");
            NormalniPacijent p3 = new NormalniPacijent(knj3);
            k17732.DodajKarton(p1.karton); k17732.DodajKarton(p2.karton); k17732.DodajKarton(p3.karton);
            k17732.RegistrujPacijenta(p2); k17732.RegistrujPacijenta(p3); k17732.RegistrujPacijenta(p1); k17732.RegistrujPacijenta(p4);
            p1.posjeta=20;
            p2.posjeta = 5;
            p3.posjeta = 15;
			
			Application.Run(f2);
        }
    }
}
