﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.Security.Cryptography;



namespace GUI
{
    

    public partial class Form1 : Form
        
    {
        private Klinika k17732 { get; set; }
        public static NormalniPacijent np { get; set; }
        public static Doktor doktor { get; set; }
        public static MedicinskiTehnicar tehnicar { get; set; }

        public Form1(Klinika k1)
        {
            InitializeComponent();
            k17732 = k1;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Prijava_Click(object sender, EventArgs e)
        {

            if (!radioButton1.Checked && !radioButton2.Checked && !radioButton3.Checked && !radioButton4.Checked)
            {
                toolStripStatusLabel1.Text = "Oznaci kruzic!";
            }
            else if (radioButton1.Checked && !radioButton2.Checked && !radioButton3.Checked && !radioButton4.Checked)
            {
                if (textBox2.Text == "admin123" && textBox1.Text == "admin")
                {
                    Form2 f2 = new Form2(k17732);
                    this.Hide();
                    f2.ShowDialog();
                    this.Close();
                }
                statusStrip1.ForeColor = Color.Red;
             //OVO JE ONO STO Se ISPISE U MENU STRIPU, GDJE SE TREBA IZABRATI MENU LABEL u dizajnu
                toolStripStatusLabel1.Text = "Pogresni podaci!";

            }


            else if (!radioButton1.Checked && radioButton2.Checked && !radioButton3.Checked && !radioButton4.Checked)
            {
                foreach (Osoblje d in k17732.osoblje)
                {
                    if (d is Doktor)
                    {
                        if (textBox1.Text == (d as Doktor).DajID() && MD5Hash(textBox2.Text) == (d as Doktor).DajLozinku())
						{ 
                            doktor = d as Doktor;
                            Form3 f = new Form3(k17732);
                            this.Hide();
                            f.ShowDialog();
                            this.Close();
                            return;
                        }

                    }

                }
                statusStrip1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "Pogresni podaci!";

            }



            else if (!radioButton1.Checked && !radioButton2.Checked && radioButton3.Checked && !radioButton4.Checked)
            {
                foreach (Osoblje d in k17732.osoblje)
                {
                    if (d is MedicinskiTehnicar)
                    {
                        if (textBox1.Text == (d as MedicinskiTehnicar).DajID() && MD5Hash(textBox2.Text) == (d as MedicinskiTehnicar).DajLozinku())
                        {
                            tehnicar = d as MedicinskiTehnicar;

                            Form5 f = new Form5(k17732);
                            this.Hide();
                            f.ShowDialog();
                            this.Close();
                            return;
                        }

                    }
                }
                statusStrip1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "Pogresni podaci!";
            }

            else if (!radioButton1.Checked && !radioButton2.Checked && !radioButton3.Checked && radioButton4.Checked)
            {
                {
                    foreach (Pacijent d in k17732.pacijenti)
                    {
                        if (d is NormalniPacijent)
                        {
                            if (textBox1.Text == (d as NormalniPacijent).DajID() && MD5Hash(textBox2.Text) == (d as NormalniPacijent).DajLozinku())
                            {
                                np = d as NormalniPacijent;
                                Form6 f = new Form6(k17732);
                                this.Hide();
                                f.ShowDialog();
                                this.Close();
                                return;
                            }

                        }

                    }

                }
                statusStrip1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "Pogresni podaci!";

            }
            
            
        }


        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void Prijava_Leave(object sender, EventArgs e)
        {
           
        }

        public bool validPass(string password, out string errorMessage)
        {
            // Provjeravamo da li je uopšte unesen mail.
            if (password=="")
            {
                errorMessage = "Potrebno unijeti lozinku.";
                return false;
            }

            // Provjeravamo da li postoji "@" i "." u email adresi i da li su u pravilnom redoslijedu.


            errorMessage = "";
            return true;
        }

        public bool validID(string password, out string errorMessage)
        {
            // Provjeravamo da li je uopšte unesen mail.
            if (password=="")
            {
                errorMessage = "Potrebno unijeti lozinku.";
                return false;
            }

            errorMessage = "";
            return true;
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {

            
            string errorMsg;
            if (!validID(textBox1.Text, out errorMsg))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                this.ID.SetError(textBox1, errorMsg);
                
            }
          


        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
			System.Drawing.Graphics mojGrafickiObjekat; // Kreiranje vlastitog Graphics objekta
			mojGrafickiObjekat = this.CreateGraphics();

			Pen p = new Pen(System.Drawing.Color.Blue, 5); // Kreiranje Pen objekta

			Rectangle pravougaonik = new Rectangle(430, 40, 100, 130);

			SolidBrush b = new SolidBrush(System.Drawing.Color.Red);
			mojGrafickiObjekat.FillRectangle(b, pravougaonik);
			SolidBrush b1 = new SolidBrush(System.Drawing.Color.White);


			Font f = new Font("Brush Script MT", 24, FontStyle.Bold);
			e.Graphics.DrawString("nmk", f, new SolidBrush(Color.White), 470, 50);

			//desni polukrug
			Rectangle p1 = new Rectangle(430, 70, 40, 40);
			mojGrafickiObjekat.FillPie(b1, p1, 90, -180);

			//pravougaonik desni
			Rectangle p3 = new Rectangle(430, 70, 20, 40);
			mojGrafickiObjekat.FillRectangle(b1, p3);



			//lijevi polukrug
			Rectangle p2 = new Rectangle(390, 70, 40, 40);
			mojGrafickiObjekat.FillPie(b, p2, 90, 180);

			//pravougaonik lijevi
			Rectangle p4 = new Rectangle(410, 70, 20, 40);
			mojGrafickiObjekat.FillRectangle(b, p4);



			//desna strana
			//lijevi polukrug
			Rectangle p5 = new Rectangle(490, 100, 40, 40);
			mojGrafickiObjekat.FillPie(b1, p5, 90, 180);

			//pravougaonik lijevi
			Rectangle p6 = new Rectangle(510, 100, 20, 40);
			mojGrafickiObjekat.FillRectangle(b1, p6);

			//desni polukrug
			Rectangle p7 = new Rectangle(530, 100, 40, 40);
			mojGrafickiObjekat.FillPie(b, p7, 90, -180);

			//pravougaonik desni
			Rectangle p8 = new Rectangle(530, 100, 20, 40);
			mojGrafickiObjekat.FillRectangle(b, p8);



			Font f1 = new Font("Brush Script MT", 16, FontStyle.Bold);
			e.Graphics.DrawString("S nama do\nozdravljenja", f1, new SolidBrush(Color.Red), 435, 170);
		}

        private void textBox1_Validated(object sender, EventArgs e)
        {
            // Ako je sve ok, obrišimo sve greške sa ErrorProvider-a.
            ID.SetError(textBox1, "");

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            
            
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            Lozinka.SetError(textBox2, "");
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;
            if (!validPass(textBox2.Text, out errorMsg))
            {

                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                this.Lozinka.SetError(textBox2, errorMsg);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k17732);
            this.Hide();
            f.ShowDialog();
            this.Close();
        }
    }
    
}
