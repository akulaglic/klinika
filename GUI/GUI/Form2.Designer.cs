﻿namespace GUI
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
			this.Doktor = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.label19 = new System.Windows.Forms.Label();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label15 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.button1 = new System.Windows.Forms.Button();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.label20 = new System.Windows.Forms.Label();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.button4 = new System.Windows.Forms.Button();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.label18 = new System.Windows.Forms.Label();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.button7 = new System.Windows.Forms.Button();
			this.label14 = new System.Windows.Forms.Label();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.button6 = new System.Windows.Forms.Button();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.doktorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tehnicarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.binarnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider3 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider4 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider5 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider6 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider7 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider8 = new System.Windows.Forms.ErrorProvider(this.components);
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.button5 = new System.Windows.Forms.Button();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.button8 = new System.Windows.Forms.Button();
			this.treeView2 = new System.Windows.Forms.TreeView();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.Doktor.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.tabPage1.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			this.tabPage3.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).BeginInit();
			this.groupBox9.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.SuspendLayout();
			// 
			// Doktor
			// 
			this.Doktor.Controls.Add(this.tabPage2);
			this.Doktor.Controls.Add(this.tabPage1);
			this.Doktor.Controls.Add(this.tabPage3);
			this.Doktor.Controls.Add(this.tabPage4);
			this.Doktor.Location = new System.Drawing.Point(12, 27);
			this.Doktor.Name = "Doktor";
			this.Doktor.SelectedIndex = 0;
			this.Doktor.Size = new System.Drawing.Size(581, 336);
			this.Doktor.TabIndex = 5;
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage2.Controls.Add(this.groupBox7);
			this.tabPage2.Controls.Add(this.groupBox2);
			this.tabPage2.Controls.Add(this.groupBox1);
			this.tabPage2.Controls.Add(this.label1);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(573, 310);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Doktor";
			this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.label19);
			this.groupBox7.Controls.Add(this.textBox11);
			this.groupBox7.Location = new System.Drawing.Point(304, 171);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(259, 133);
			this.groupBox7.TabIndex = 8;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Spremanje";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(15, 52);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(74, 13);
			this.label19.TabIndex = 7;
			this.label19.Text = "Ime Datoteke:";
			// 
			// textBox11
			// 
			this.textBox11.Location = new System.Drawing.Point(93, 49);
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new System.Drawing.Size(160, 20);
			this.textBox11.TabIndex = 6;
			// 
			// groupBox2
			// 
			this.groupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox2.Controls.Add(this.button2);
			this.groupBox2.Controls.Add(this.textBox4);
			this.groupBox2.Controls.Add(this.textBox3);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox2.Location = new System.Drawing.Point(304, 6);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(259, 158);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Brisanje";
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button2.Location = new System.Drawing.Point(93, 125);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(118, 27);
			this.button2.TabIndex = 5;
			this.button2.Text = "Obrisi";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(71, 82);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(160, 21);
			this.textBox4.TabIndex = 4;
			this.textBox4.Validating += new System.ComponentModel.CancelEventHandler(this.textBox4_Validating);
			this.textBox4.Validated += new System.EventHandler(this.textBox4_Validated);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(71, 38);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(160, 21);
			this.textBox3.TabIndex = 3;
			this.textBox3.Validating += new System.ComponentModel.CancelEventHandler(this.textBox3_Validating);
			this.textBox3.Validated += new System.EventHandler(this.textBox3_Validated);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(13, 82);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 15);
			this.label5.TabIndex = 2;
			this.label5.Text = "Prezime:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(38, 35);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(31, 15);
			this.label4.TabIndex = 1;
			this.label4.Text = "Ime:";
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox1.Controls.Add(this.comboBox1);
			this.groupBox1.Controls.Add(this.label15);
			this.groupBox1.Controls.Add(this.numericUpDown1);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.dateTimePicker1);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox1.Location = new System.Drawing.Point(6, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(292, 296);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Unos";
			this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(89, 101);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(176, 23);
			this.comboBox1.TabIndex = 10;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(21, 104);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(66, 15);
			this.label15.TabIndex = 9;
			this.label15.Text = "Ordinacija:";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(89, 141);
			this.numericUpDown1.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(176, 21);
			this.numericUpDown1.TabIndex = 8;
			this.numericUpDown1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button1.Location = new System.Drawing.Point(93, 250);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(108, 29);
			this.button1.TabIndex = 0;
			this.button1.Text = "Registruj";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(93, 192);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(172, 21);
			this.dateTimePicker1.TabIndex = 0;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(89, 64);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(176, 21);
			this.textBox2.TabIndex = 6;
			this.textBox2.Validating += new System.ComponentModel.CancelEventHandler(this.textBox2_Validating);
			this.textBox2.Validated += new System.EventHandler(this.textBox2_Validated);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(1, 197);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(86, 15);
			this.label7.TabIndex = 5;
			this.label7.Text = "Datum Unosa:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(49, 143);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(38, 15);
			this.label6.TabIndex = 4;
			this.label6.Text = "Plata:";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(89, 28);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(176, 21);
			this.textBox1.TabIndex = 0;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.textBox1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox1_Validating);
			this.textBox1.Validated += new System.EventHandler(this.textBox1_Validated);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(31, 67);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 15);
			this.label3.TabIndex = 1;
			this.label3.Text = "Prezime:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(56, 31);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(31, 15);
			this.label2.TabIndex = 0;
			this.label2.Text = "Ime:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(29, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(0, 13);
			this.label1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage1.Controls.Add(this.groupBox8);
			this.tabPage1.Controls.Add(this.groupBox4);
			this.tabPage1.Controls.Add(this.groupBox3);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(573, 310);
			this.tabPage1.TabIndex = 3;
			this.tabPage1.Text = "Tehnicar";
			this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.label20);
			this.groupBox8.Controls.Add(this.textBox12);
			this.groupBox8.Location = new System.Drawing.Point(306, 177);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(252, 127);
			this.groupBox8.TabIndex = 9;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Spremanje";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(7, 52);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(74, 13);
			this.label20.TabIndex = 7;
			this.label20.Text = "Ime Datoteke:";
			// 
			// textBox12
			// 
			this.textBox12.Location = new System.Drawing.Point(86, 49);
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new System.Drawing.Size(160, 20);
			this.textBox12.TabIndex = 6;
			// 
			// groupBox4
			// 
			this.groupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox4.Controls.Add(this.button4);
			this.groupBox4.Controls.Add(this.textBox7);
			this.groupBox4.Controls.Add(this.textBox8);
			this.groupBox4.Controls.Add(this.label12);
			this.groupBox4.Controls.Add(this.label13);
			this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox4.Location = new System.Drawing.Point(307, 7);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(251, 155);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Brisanje";
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button4.Location = new System.Drawing.Point(85, 117);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(112, 25);
			this.button4.TabIndex = 5;
			this.button4.Text = "Obrisi";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(68, 79);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(150, 21);
			this.textBox7.TabIndex = 4;
			this.textBox7.Validating += new System.ComponentModel.CancelEventHandler(this.textBox7_Validating);
			this.textBox7.Validated += new System.EventHandler(this.textBox7_Validated);
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(68, 32);
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(150, 21);
			this.textBox8.TabIndex = 3;
			this.textBox8.Validating += new System.ComponentModel.CancelEventHandler(this.textBox8_Validating);
			this.textBox8.Validated += new System.EventHandler(this.textBox8_Validated);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 82);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(56, 15);
			this.label12.TabIndex = 2;
			this.label12.Text = "Prezime:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(31, 38);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(31, 15);
			this.label13.TabIndex = 1;
			this.label13.Text = "Ime:";
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox3.Controls.Add(this.comboBox2);
			this.groupBox3.Controls.Add(this.label17);
			this.groupBox3.Controls.Add(this.label16);
			this.groupBox3.Controls.Add(this.button3);
			this.groupBox3.Controls.Add(this.numericUpDown2);
			this.groupBox3.Controls.Add(this.dateTimePicker2);
			this.groupBox3.Controls.Add(this.textBox5);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.textBox6);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.groupBox3.Location = new System.Drawing.Point(6, 6);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(295, 298);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Unos";
			// 
			// comboBox2
			// 
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Location = new System.Drawing.Point(89, 96);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(166, 23);
			this.comboBox2.TabIndex = 12;
			this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(17, 99);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(66, 15);
			this.label17.TabIndex = 11;
			this.label17.Text = "Ordinacija:";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(125, 149);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(0, 15);
			this.label16.TabIndex = 10;
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button3.Location = new System.Drawing.Point(89, 242);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(114, 37);
			this.button3.TabIndex = 0;
			this.button3.Text = "Registruj";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(89, 135);
			this.numericUpDown2.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(166, 21);
			this.numericUpDown2.TabIndex = 0;
			this.numericUpDown2.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Location = new System.Drawing.Point(89, 186);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(166, 21);
			this.dateTimePicker2.TabIndex = 0;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(89, 63);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(166, 21);
			this.textBox5.TabIndex = 6;
			this.textBox5.Validating += new System.ComponentModel.CancelEventHandler(this.textBox5_Validating);
			this.textBox5.Validated += new System.EventHandler(this.textBox5_Validated);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(1, 186);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(86, 15);
			this.label8.TabIndex = 5;
			this.label8.Text = "Datum Unosa:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(42, 137);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(38, 15);
			this.label9.TabIndex = 4;
			this.label9.Text = "Plata:";
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(89, 28);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(166, 21);
			this.textBox6.TabIndex = 0;
			this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
			this.textBox6.Validating += new System.ComponentModel.CancelEventHandler(this.textBox6_Validating);
			this.textBox6.Validated += new System.EventHandler(this.textBox6_Validated);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(24, 66);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(56, 15);
			this.label10.TabIndex = 1;
			this.label10.Text = "Prezime:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(49, 31);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(31, 15);
			this.label11.TabIndex = 0;
			this.label11.Text = "Ime:";
			// 
			// tabPage3
			// 
			this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage3.Controls.Add(this.groupBox6);
			this.tabPage3.Controls.Add(this.groupBox5);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(573, 310);
			this.tabPage3.TabIndex = 4;
			this.tabPage3.Text = "Ordinacija";
			// 
			// groupBox6
			// 
			this.groupBox6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox6.Controls.Add(this.listBox1);
			this.groupBox6.Controls.Add(this.label18);
			this.groupBox6.Controls.Add(this.textBox10);
			this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox6.Location = new System.Drawing.Point(27, 107);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(502, 172);
			this.groupBox6.TabIndex = 4;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Pretraga";
			// 
			// listBox1
			// 
			this.listBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 15;
			this.listBox1.Location = new System.Drawing.Point(72, 71);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(230, 94);
			this.listBox1.TabIndex = 3;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(29, 35);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(40, 15);
			this.label18.TabIndex = 2;
			this.label18.Text = "Naziv:";
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(72, 32);
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new System.Drawing.Size(230, 21);
			this.textBox10.TabIndex = 1;
			this.textBox10.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
			// 
			// groupBox5
			// 
			this.groupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.groupBox5.Controls.Add(this.button7);
			this.groupBox5.Controls.Add(this.label14);
			this.groupBox5.Controls.Add(this.textBox9);
			this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.groupBox5.Location = new System.Drawing.Point(27, 18);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(502, 83);
			this.groupBox5.TabIndex = 3;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Dodaj";
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button7.Location = new System.Drawing.Point(358, 32);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(86, 30);
			this.button7.TabIndex = 0;
			this.button7.Text = "Dodaj";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(29, 35);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(40, 15);
			this.label14.TabIndex = 2;
			this.label14.Text = "Naziv:";
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(72, 32);
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new System.Drawing.Size(230, 21);
			this.textBox9.TabIndex = 1;
			this.textBox9.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
			// 
			// tabPage4
			// 
			this.tabPage4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage4.Controls.Add(this.button6);
			this.tabPage4.Controls.Add(this.richTextBox1);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(573, 310);
			this.tabPage4.TabIndex = 5;
			this.tabPage4.Text = "Logovi";
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button6.Location = new System.Drawing.Point(28, 274);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(171, 24);
			this.button6.TabIndex = 1;
			this.button6.Text = "Prikazi Izuzetke iz Datoteke";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.button6_Click_1);
			// 
			// richTextBox1
			// 
			this.richTextBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.richTextBox1.Location = new System.Drawing.Point(28, 24);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(506, 234);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = "";
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(938, 24);
			this.menuStrip1.TabIndex = 6;
			this.menuStrip1.Text = "menuStrip1";
			this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.saveAsToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doktorToolStripMenuItem,
            this.tehnicarToolStripMenuItem});
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.newToolStripMenuItem.Text = "New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// doktorToolStripMenuItem
			// 
			this.doktorToolStripMenuItem.Name = "doktorToolStripMenuItem";
			this.doktorToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.doktorToolStripMenuItem.Text = "Doktor";
			this.doktorToolStripMenuItem.Click += new System.EventHandler(this.doktorToolStripMenuItem_Click);
			// 
			// tehnicarToolStripMenuItem
			// 
			this.tehnicarToolStripMenuItem.Name = "tehnicarToolStripMenuItem";
			this.tehnicarToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.tehnicarToolStripMenuItem.Text = "Tehnicar";
			this.tehnicarToolStripMenuItem.Click += new System.EventHandler(this.tehnicarToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarnoToolStripMenuItem,
            this.xMLToolStripMenuItem});
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.saveAsToolStripMenuItem.Text = "Save As...";
			// 
			// binarnoToolStripMenuItem
			// 
			this.binarnoToolStripMenuItem.Name = "binarnoToolStripMenuItem";
			this.binarnoToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
			this.binarnoToolStripMenuItem.Text = "Binarno";
			this.binarnoToolStripMenuItem.Click += new System.EventHandler(this.binarnoToolStripMenuItem_Click);
			// 
			// xMLToolStripMenuItem
			// 
			this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
			this.xMLToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
			this.xMLToolStripMenuItem.Text = "XML";
			this.xMLToolStripMenuItem.Click += new System.EventHandler(this.xMLToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sizeToolStripMenuItem,
            this.colorToolStripMenuItem,
            this.propertiesToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// sizeToolStripMenuItem
			// 
			this.sizeToolStripMenuItem.Name = "sizeToolStripMenuItem";
			this.sizeToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
			this.sizeToolStripMenuItem.Text = "Layout";
			// 
			// colorToolStripMenuItem
			// 
			this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
			this.colorToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
			this.colorToolStripMenuItem.Text = "Color";
			// 
			// propertiesToolStripMenuItem
			// 
			this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
			this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
			this.propertiesToolStripMenuItem.Text = "Properties";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// statusStrip1
			// 
			this.statusStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
			this.statusStrip1.Location = new System.Drawing.Point(0, 368);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(938, 22);
			this.statusStrip1.TabIndex = 7;
			this.statusStrip1.Text = "statusStrip1";
			this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
			// 
			// errorProvider1
			// 
			this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider1.ContainerControl = this;
			// 
			// errorProvider2
			// 
			this.errorProvider2.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider2.ContainerControl = this;
			// 
			// errorProvider3
			// 
			this.errorProvider3.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider3.ContainerControl = this;
			// 
			// errorProvider4
			// 
			this.errorProvider4.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider4.ContainerControl = this;
			// 
			// errorProvider5
			// 
			this.errorProvider5.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider5.ContainerControl = this;
			// 
			// errorProvider6
			// 
			this.errorProvider6.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider6.ContainerControl = this;
			// 
			// errorProvider7
			// 
			this.errorProvider7.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider7.ContainerControl = this;
			// 
			// errorProvider8
			// 
			this.errorProvider8.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider8.ContainerControl = this;
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.button5);
			this.groupBox9.Controls.Add(this.treeView1);
			this.groupBox9.Location = new System.Drawing.Point(599, 55);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(166, 298);
			this.groupBox9.TabIndex = 8;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Binarni Prikaz";
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button5.Location = new System.Drawing.Point(31, 268);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(112, 24);
			this.button5.TabIndex = 0;
			this.button5.Text = "Prikazi";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.button5_Click_1);
			// 
			// treeView1
			// 
			this.treeView1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.treeView1.Location = new System.Drawing.Point(10, 18);
			this.treeView1.Name = "treeView1";
			this.treeView1.Size = new System.Drawing.Size(150, 234);
			this.treeView1.TabIndex = 9;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.button8);
			this.groupBox10.Controls.Add(this.treeView2);
			this.groupBox10.Location = new System.Drawing.Point(772, 56);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(166, 298);
			this.groupBox10.TabIndex = 9;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "XML Prikaz";
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button8.Location = new System.Drawing.Point(31, 268);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(112, 24);
			this.button8.TabIndex = 0;
			this.button8.Text = "Prikazi";
			this.button8.UseVisualStyleBackColor = false;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// treeView2
			// 
			this.treeView2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.treeView2.Location = new System.Drawing.Point(10, 18);
			this.treeView2.Name = "treeView2";
			this.treeView2.Size = new System.Drawing.Size(150, 234);
			this.treeView2.TabIndex = 9;
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
			this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
			// 
			// Form2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.ClientSize = new System.Drawing.Size(938, 390);
			this.Controls.Add(this.groupBox10);
			this.Controls.Add(this.groupBox9);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.Doktor);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Form2";
			this.Text = "Admin";
			this.Load += new System.EventHandler(this.Form2_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form2_Paint);
			this.Doktor.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.groupBox7.ResumeLayout(false);
			this.groupBox7.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.tabPage1.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox8.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			this.tabPage3.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.tabPage4.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).EndInit();
			this.groupBox9.ResumeLayout(false);
			this.groupBox10.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl Doktor;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doktorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tehnicarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.ErrorProvider errorProvider3;
        private System.Windows.Forms.ErrorProvider errorProvider4;
        private System.Windows.Forms.ErrorProvider errorProvider5;
        private System.Windows.Forms.ErrorProvider errorProvider6;
        private System.Windows.Forms.ErrorProvider errorProvider7;
        private System.Windows.Forms.ErrorProvider errorProvider8;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem binarnoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.TreeView treeView2;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
	}
}