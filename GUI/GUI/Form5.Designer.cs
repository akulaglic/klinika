﻿namespace GUI
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.registrujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.naruciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.anamnezaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.hitnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button6 = new System.Windows.Forms.Button();
			this.label13 = new System.Windows.Forms.Label();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.button3 = new System.Windows.Forms.Button();
			this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.button5 = new System.Windows.Forms.Button();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.label9 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.button4 = new System.Windows.Forms.Button();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.richTextBox3 = new System.Windows.Forms.RichTextBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.button2 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.label18 = new System.Windows.Forms.Label();
			this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
			this.label17 = new System.Windows.Forms.Label();
			this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.richTextBox4 = new System.Windows.Forms.RichTextBox();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.richTextBox5 = new System.Windows.Forms.RichTextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider3 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider4 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider5 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider6 = new System.Windows.Forms.ErrorProvider(this.components);
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.errorProvider7 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider8 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider9 = new System.Windows.Forms.ErrorProvider(this.components);
			this.errorProvider10 = new System.Windows.Forms.ErrorProvider(this.components);
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.JMBG = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Grad = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabPage6 = new System.Windows.Forms.TabPage();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.button7 = new System.Windows.Forms.Button();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.binarnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.slika1 = new GUI.Slika();
			this.menuStrip1.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.groupBox9.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).BeginInit();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider10)).BeginInit();
			this.tabPage5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.tabPage6.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem1,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(575, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.editToolStripMenuItem,
            this.saveAsToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrujToolStripMenuItem,
            this.naruciToolStripMenuItem,
            this.anamnezaToolStripMenuItem,
            this.hitnoToolStripMenuItem});
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.newToolStripMenuItem.Text = "New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// registrujToolStripMenuItem
			// 
			this.registrujToolStripMenuItem.Name = "registrujToolStripMenuItem";
			this.registrujToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.registrujToolStripMenuItem.Text = "Registruj";
			this.registrujToolStripMenuItem.Click += new System.EventHandler(this.registrujToolStripMenuItem_Click);
			// 
			// naruciToolStripMenuItem
			// 
			this.naruciToolStripMenuItem.Name = "naruciToolStripMenuItem";
			this.naruciToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.naruciToolStripMenuItem.Text = "Naruci";
			this.naruciToolStripMenuItem.Click += new System.EventHandler(this.naruciToolStripMenuItem_Click);
			// 
			// anamnezaToolStripMenuItem
			// 
			this.anamnezaToolStripMenuItem.Name = "anamnezaToolStripMenuItem";
			this.anamnezaToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.anamnezaToolStripMenuItem.Text = "Anamneza";
			this.anamnezaToolStripMenuItem.Click += new System.EventHandler(this.anamnezaToolStripMenuItem_Click);
			// 
			// hitnoToolStripMenuItem
			// 
			this.hitnoToolStripMenuItem.Name = "hitnoToolStripMenuItem";
			this.hitnoToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.hitnoToolStripMenuItem.Text = "Hitno";
			this.hitnoToolStripMenuItem.Click += new System.EventHandler(this.hitnoToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.editToolStripMenuItem.Text = "Exit";
			this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem1
			// 
			this.editToolStripMenuItem1.Name = "editToolStripMenuItem1";
			this.editToolStripMenuItem1.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem1.Text = "Edit";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// tabPage3
			// 
			this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage3.Controls.Add(this.groupBox2);
			this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(562, 318);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Naruči Pacijenta";
			this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.button6);
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.textBox7);
			this.groupBox2.Controls.Add(this.comboBox2);
			this.groupBox2.Controls.Add(this.button3);
			this.groupBox2.Controls.Add(this.dateTimePicker3);
			this.groupBox2.Controls.Add(this.textBox6);
			this.groupBox2.Controls.Add(this.textBox5);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.label12);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Location = new System.Drawing.Point(8, 21);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(536, 280);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Naruči";
			this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.button6.Location = new System.Drawing.Point(350, 93);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(128, 28);
			this.button6.TabIndex = 27;
			this.button6.Text = "Prikazi Raspored";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(67, 119);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(44, 15);
			this.label13.TabIndex = 26;
			this.label13.Text = "JMBG:";
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(117, 116);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(158, 21);
			this.textBox7.TabIndex = 25;
			this.textBox7.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
			this.textBox7.Validating += new System.ComponentModel.CancelEventHandler(this.textBox7_Validating);
			this.textBox7.Validated += new System.EventHandler(this.textBox7_Validated);
			// 
			// comboBox2
			// 
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Location = new System.Drawing.Point(117, 158);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(158, 23);
			this.comboBox2.TabIndex = 24;
			this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button3.Location = new System.Drawing.Point(365, 137);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(98, 27);
			this.button3.TabIndex = 23;
			this.button3.Text = "Naruči";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// dateTimePicker3
			// 
			this.dateTimePicker3.Location = new System.Drawing.Point(117, 205);
			this.dateTimePicker3.Name = "dateTimePicker3";
			this.dateTimePicker3.Size = new System.Drawing.Size(158, 21);
			this.dateTimePicker3.TabIndex = 21;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(117, 74);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(158, 21);
			this.textBox6.TabIndex = 19;
			this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
			this.textBox6.Validating += new System.ComponentModel.CancelEventHandler(this.textBox6_Validating);
			this.textBox6.Validated += new System.EventHandler(this.textBox6_Validated);
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(117, 31);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(158, 21);
			this.textBox5.TabIndex = 18;
			this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
			this.textBox5.Validating += new System.ComponentModel.CancelEventHandler(this.textBox5_Validating);
			this.textBox5.Validated += new System.EventHandler(this.textBox5_Validated);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(80, 31);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(31, 15);
			this.label7.TabIndex = 1;
			this.label7.Text = "Ime:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(15, 210);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(96, 15);
			this.label12.TabIndex = 10;
			this.label12.Text = "Datum Termina:";
			this.label12.Click += new System.EventHandler(this.label12_Click);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(55, 80);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(56, 15);
			this.label10.TabIndex = 2;
			this.label10.Text = "Prezime:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(45, 166);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(66, 15);
			this.label11.TabIndex = 3;
			this.label11.Text = "Ordinacija:";
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage1.Controls.Add(this.label19);
			this.tabPage1.Controls.Add(this.textBox10);
			this.tabPage1.Controls.Add(this.slika1);
			this.tabPage1.Controls.Add(this.comboBox3);
			this.tabPage1.Controls.Add(this.button5);
			this.tabPage1.Controls.Add(this.textBox3);
			this.tabPage1.Controls.Add(this.textBox2);
			this.tabPage1.Controls.Add(this.textBox1);
			this.tabPage1.Controls.Add(this.comboBox1);
			this.tabPage1.Controls.Add(this.dateTimePicker2);
			this.tabPage1.Controls.Add(this.dateTimePicker1);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Controls.Add(this.label9);
			this.tabPage1.Controls.Add(this.button1);
			this.tabPage1.Controls.Add(this.label8);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(562, 318);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Registruj Pacijenta";
			this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
			// 
			// comboBox3
			// 
			this.comboBox3.FormattingEnabled = true;
			this.comboBox3.Items.AddRange(new object[] {
            "udata",
            "neudata",
            "ozenjen",
            "neozenjen",
            "razveden/a"});
			this.comboBox3.Location = new System.Drawing.Point(111, 146);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(188, 23);
			this.comboBox3.TabIndex = 23;
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button5.Location = new System.Drawing.Point(446, 286);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(98, 23);
			this.button5.TabIndex = 22;
			this.button5.Text = "Obriši";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(111, 63);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(188, 21);
			this.textBox3.TabIndex = 19;
			this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
			this.textBox3.Validating += new System.ComponentModel.CancelEventHandler(this.textBox3_Validating);
			this.textBox3.Validated += new System.EventHandler(this.textBox3_Validated);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(111, 36);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(188, 21);
			this.textBox2.TabIndex = 18;
			this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
			this.textBox2.Validating += new System.ComponentModel.CancelEventHandler(this.textBox2_Validating);
			this.textBox2.Validated += new System.EventHandler(this.textBox2_Validated);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(111, 9);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(188, 21);
			this.textBox1.TabIndex = 17;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.textBox1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox1_Validating);
			this.textBox1.Validated += new System.EventHandler(this.textBox1_Validated);
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
            "Sarajevo",
            "Banja Luka",
            "Zenica",
            "Bihać",
            "Mostar",
            "Tuzla",
            "Brčko",
            "Doboj"});
			this.comboBox1.Location = new System.Drawing.Point(111, 117);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(188, 23);
			this.comboBox1.TabIndex = 20;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Location = new System.Drawing.Point(111, 90);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(188, 21);
			this.dateTimePicker2.TabIndex = 16;
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(111, 245);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(188, 21);
			this.dateTimePicker1.TabIndex = 15;
			this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButton1);
			this.groupBox1.Controls.Add(this.radioButton2);
			this.groupBox1.Location = new System.Drawing.Point(111, 182);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(188, 52);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Spol";
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Location = new System.Drawing.Point(22, 19);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(62, 19);
			this.radioButton1.TabIndex = 6;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Musko";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.AutoSize = true;
			this.radioButton2.Location = new System.Drawing.Point(97, 19);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(65, 19);
			this.radioButton2.TabIndex = 7;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Zensko";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(367, 152);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(0, 15);
			this.label9.TabIndex = 11;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button1.Location = new System.Drawing.Point(325, 286);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(98, 23);
			this.button1.TabIndex = 10;
			this.button1.Text = "Kreiraj Karton";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(8, 245);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(99, 15);
			this.label8.TabIndex = 9;
			this.label8.Text = "Današnji Datum:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(20, 149);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(87, 15);
			this.label6.TabIndex = 5;
			this.label6.Text = "Bračno Stanje:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(70, 120);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(37, 15);
			this.label5.TabIndex = 4;
			this.label5.Text = "Grad:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(8, 95);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(97, 15);
			this.label4.TabIndex = 3;
			this.label4.Text = "Datum Rođenja:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(61, 69);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 15);
			this.label3.TabIndex = 2;
			this.label3.Text = "JMBG:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(49, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Prezime:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(74, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(31, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Ime:";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Controls.Add(this.tabPage6);
			this.tabControl1.Location = new System.Drawing.Point(5, 27);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(570, 344);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage2.Controls.Add(this.button4);
			this.tabPage2.Controls.Add(this.groupBox7);
			this.tabPage2.Controls.Add(this.groupBox6);
			this.tabPage2.Controls.Add(this.groupBox4);
			this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(562, 318);
			this.tabPage2.TabIndex = 3;
			this.tabPage2.Text = "Anamneza";
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button4.Location = new System.Drawing.Point(445, 254);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(97, 27);
			this.button4.TabIndex = 0;
			this.button4.Text = "Unesi";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.richTextBox3);
			this.groupBox7.Location = new System.Drawing.Point(6, 218);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(417, 93);
			this.groupBox7.TabIndex = 3;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Porodične Bolesti";
			// 
			// richTextBox3
			// 
			this.richTextBox3.Location = new System.Drawing.Point(8, 19);
			this.richTextBox3.Name = "richTextBox3";
			this.richTextBox3.Size = new System.Drawing.Size(389, 68);
			this.richTextBox3.TabIndex = 3;
			this.richTextBox3.Text = "";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.richTextBox2);
			this.groupBox6.Location = new System.Drawing.Point(8, 113);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(534, 93);
			this.groupBox6.TabIndex = 2;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Alergije";
			// 
			// richTextBox2
			// 
			this.richTextBox2.Location = new System.Drawing.Point(6, 18);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(506, 69);
			this.richTextBox2.TabIndex = 3;
			this.richTextBox2.Text = "";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.richTextBox1);
			this.groupBox4.Controls.Add(this.groupBox5);
			this.groupBox4.Location = new System.Drawing.Point(8, 6);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(534, 100);
			this.groupBox4.TabIndex = 0;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Sadašnja Bolest";
			this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
			// 
			// richTextBox1
			// 
			this.richTextBox1.Location = new System.Drawing.Point(6, 19);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(506, 75);
			this.richTextBox1.TabIndex = 2;
			this.richTextBox1.Text = "";
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// groupBox5
			// 
			this.groupBox5.Location = new System.Drawing.Point(0, 106);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(534, 100);
			this.groupBox5.TabIndex = 1;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Alergije";
			// 
			// tabPage4
			// 
			this.tabPage4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage4.Controls.Add(this.button2);
			this.tabPage4.Controls.Add(this.checkBox1);
			this.tabPage4.Controls.Add(this.groupBox10);
			this.tabPage4.Controls.Add(this.groupBox9);
			this.tabPage4.Controls.Add(this.groupBox8);
			this.tabPage4.Controls.Add(this.groupBox3);
			this.tabPage4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(562, 318);
			this.tabPage4.TabIndex = 4;
			this.tabPage4.Text = "Hitno";
			this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button2.Location = new System.Drawing.Point(437, 276);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(104, 27);
			this.button2.TabIndex = 7;
			this.button2.Text = "Unesi";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click_1);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(283, 132);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(80, 19);
			this.checkBox1.TabIndex = 6;
			this.checkBox1.Text = "Preminuo";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.label18);
			this.groupBox10.Controls.Add(this.dateTimePicker5);
			this.groupBox10.Controls.Add(this.label17);
			this.groupBox10.Controls.Add(this.dateTimePicker4);
			this.groupBox10.Location = new System.Drawing.Point(277, 155);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(279, 115);
			this.groupBox10.TabIndex = 2;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "Info";
			this.groupBox10.Visible = false;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(3, 72);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(103, 15);
			this.label18.TabIndex = 9;
			this.label18.Text = "Datum obdukcije:\r\n";
			// 
			// dateTimePicker5
			// 
			this.dateTimePicker5.Location = new System.Drawing.Point(109, 72);
			this.dateTimePicker5.Name = "dateTimePicker5";
			this.dateTimePicker5.Size = new System.Drawing.Size(164, 21);
			this.dateTimePicker5.TabIndex = 8;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(21, 40);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(82, 15);
			this.label17.TabIndex = 7;
			this.label17.Text = "Vrijeme smrti:";
			this.label17.Click += new System.EventHandler(this.label17_Click);
			// 
			// dateTimePicker4
			// 
			this.dateTimePicker4.Location = new System.Drawing.Point(108, 35);
			this.dateTimePicker4.Name = "dateTimePicker4";
			this.dateTimePicker4.Size = new System.Drawing.Size(165, 21);
			this.dateTimePicker4.TabIndex = 5;
			this.dateTimePicker4.ValueChanged += new System.EventHandler(this.dateTimePicker4_ValueChanged_1);
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.richTextBox4);
			this.groupBox9.Location = new System.Drawing.Point(8, 146);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(263, 163);
			this.groupBox9.TabIndex = 0;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Prva pomoć";
			// 
			// richTextBox4
			// 
			this.richTextBox4.Location = new System.Drawing.Point(6, 19);
			this.richTextBox4.Name = "richTextBox4";
			this.richTextBox4.Size = new System.Drawing.Size(251, 138);
			this.richTextBox4.TabIndex = 0;
			this.richTextBox4.Text = "";
			this.richTextBox4.Validating += new System.ComponentModel.CancelEventHandler(this.richTextBox4_Validating);
			this.richTextBox4.Validated += new System.EventHandler(this.richTextBox4_Validated);
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.richTextBox5);
			this.groupBox8.Location = new System.Drawing.Point(277, 6);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(279, 120);
			this.groupBox8.TabIndex = 4;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Uzrok nesreće";
			// 
			// richTextBox5
			// 
			this.richTextBox5.Location = new System.Drawing.Point(6, 19);
			this.richTextBox5.Name = "richTextBox5";
			this.richTextBox5.Size = new System.Drawing.Size(267, 95);
			this.richTextBox5.TabIndex = 1;
			this.richTextBox5.Text = "";
			this.richTextBox5.Validating += new System.ComponentModel.CancelEventHandler(this.richTextBox5_Validating);
			this.richTextBox5.Validated += new System.EventHandler(this.richTextBox5_Validated);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label16);
			this.groupBox3.Controls.Add(this.label15);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.textBox4);
			this.groupBox3.Controls.Add(this.textBox9);
			this.groupBox3.Controls.Add(this.textBox8);
			this.groupBox3.Location = new System.Drawing.Point(8, 6);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(263, 134);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Podaci";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(30, 93);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(44, 15);
			this.label16.TabIndex = 5;
			this.label16.Text = "JMBG:";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(20, 59);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(56, 15);
			this.label15.TabIndex = 4;
			this.label15.Text = "Prezime:";
			this.label15.Click += new System.EventHandler(this.label15_Click);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(43, 26);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(31, 15);
			this.label14.TabIndex = 3;
			this.label14.Text = "Ime:";
			this.label14.Click += new System.EventHandler(this.label14_Click);
			// 
			// textBox4
			// 
			this.textBox4.AccessibleName = "";
			this.textBox4.Location = new System.Drawing.Point(82, 23);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(129, 21);
			this.textBox4.TabIndex = 0;
			this.textBox4.Tag = "Ime";
			this.textBox4.Validating += new System.ComponentModel.CancelEventHandler(this.textBox4_Validating);
			this.textBox4.Validated += new System.EventHandler(this.textBox4_Validated);
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(82, 90);
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new System.Drawing.Size(129, 21);
			this.textBox9.TabIndex = 2;
			this.textBox9.Validating += new System.ComponentModel.CancelEventHandler(this.textBox9_Validating);
			this.textBox9.Validated += new System.EventHandler(this.textBox9_Validated);
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(82, 56);
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(129, 21);
			this.textBox8.TabIndex = 1;
			this.textBox8.Validating += new System.ComponentModel.CancelEventHandler(this.textBox8_Validating);
			this.textBox8.Validated += new System.EventHandler(this.textBox8_Validated);
			// 
			// folderBrowserDialog1
			// 
			this.folderBrowserDialog1.SelectedPath = "C:\\Users\\Azra\\Desktop";
			this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// errorProvider2
			// 
			this.errorProvider2.ContainerControl = this;
			// 
			// errorProvider3
			// 
			this.errorProvider3.ContainerControl = this;
			// 
			// errorProvider4
			// 
			this.errorProvider4.ContainerControl = this;
			// 
			// errorProvider5
			// 
			this.errorProvider5.ContainerControl = this;
			// 
			// errorProvider6
			// 
			this.errorProvider6.ContainerControl = this;
			// 
			// statusStrip1
			// 
			this.statusStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 361);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(575, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
			// 
			// errorProvider7
			// 
			this.errorProvider7.ContainerControl = this;
			// 
			// errorProvider8
			// 
			this.errorProvider8.ContainerControl = this;
			// 
			// errorProvider9
			// 
			this.errorProvider9.ContainerControl = this;
			// 
			// errorProvider10
			// 
			this.errorProvider10.ContainerControl = this;
			// 
			// tabPage5
			// 
			this.tabPage5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage5.Controls.Add(this.dataGridView1);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(562, 318);
			this.tabPage5.TabIndex = 5;
			this.tabPage5.Text = "XML";
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ime,
            this.Column1,
            this.Column2,
            this.Prezime,
            this.JMBG,
            this.Grad,
            this.Column3});
			this.dataGridView1.Location = new System.Drawing.Point(9, 45);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(547, 210);
			this.dataGridView1.TabIndex = 0;
			// 
			// Ime
			// 
			this.Ime.HeaderText = "Ime";
			this.Ime.Name = "Ime";
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Prezime";
			this.Column1.Name = "Column1";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "JMBG";
			this.Column2.Name = "Column2";
			// 
			// Prezime
			// 
			this.Prezime.HeaderText = "Grad";
			this.Prezime.Name = "Prezime";
			// 
			// JMBG
			// 
			this.JMBG.HeaderText = "Datum Rodjenja";
			this.JMBG.Name = "JMBG";
			// 
			// Grad
			// 
			this.Grad.HeaderText = "Spol";
			this.Grad.Name = "Grad";
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Bracno Stanje";
			this.Column3.Name = "Column3";
			// 
			// tabPage6
			// 
			this.tabPage6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.tabPage6.Controls.Add(this.button7);
			this.tabPage6.Controls.Add(this.treeView1);
			this.tabPage6.Location = new System.Drawing.Point(4, 22);
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage6.Size = new System.Drawing.Size(562, 318);
			this.tabPage6.TabIndex = 6;
			this.tabPage6.Text = "Binarna";
			// 
			// treeView1
			// 
			this.treeView1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.treeView1.Location = new System.Drawing.Point(57, 22);
			this.treeView1.Name = "treeView1";
			this.treeView1.Size = new System.Drawing.Size(432, 240);
			this.treeView1.TabIndex = 0;
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.button7.Location = new System.Drawing.Point(57, 268);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(91, 27);
			this.button7.TabIndex = 1;
			this.button7.Text = "Prikazi";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarnoToolStripMenuItem,
            this.xMLToolStripMenuItem});
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.saveAsToolStripMenuItem.Text = "Save As...";
			// 
			// binarnoToolStripMenuItem
			// 
			this.binarnoToolStripMenuItem.Name = "binarnoToolStripMenuItem";
			this.binarnoToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
			this.binarnoToolStripMenuItem.Text = "Binarno";
			this.binarnoToolStripMenuItem.Click += new System.EventHandler(this.binarnoToolStripMenuItem_Click);
			// 
			// xMLToolStripMenuItem
			// 
			this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
			this.xMLToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
			this.xMLToolStripMenuItem.Text = "XML";
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(111, 280);
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new System.Drawing.Size(188, 21);
			this.textBox10.TabIndex = 26;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(24, 280);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(83, 15);
			this.label19.TabIndex = 27;
			this.label19.Text = "Ime Datoteke:";
			// 
			// slika1
			// 
			this.slika1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.slika1.Location = new System.Drawing.Point(314, 3);
			this.slika1.Name = "slika1";
			this.slika1.Size = new System.Drawing.Size(257, 274);
			this.slika1.TabIndex = 25;
			this.slika1.Load += new System.EventHandler(this.slika1_Load);
			this.slika1.Validating += new System.ComponentModel.CancelEventHandler(this.slika1_Validating);
			this.slika1.Validated += new System.EventHandler(this.slika1_Validated);
			// 
			// Form5
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.ClientSize = new System.Drawing.Size(575, 383);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form5";
			this.Text = "Medicinski Tehničar";
			this.Load += new System.EventHandler(this.Form5_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			this.groupBox10.ResumeLayout(false);
			this.groupBox10.PerformLayout();
			this.groupBox9.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider6)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider10)).EndInit();
			this.tabPage5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.tabPage6.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private Slika slika1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.ErrorProvider errorProvider3;
        private System.Windows.Forms.ErrorProvider errorProvider4;
        private System.Windows.Forms.ErrorProvider errorProvider5;
        private System.Windows.Forms.ErrorProvider errorProvider6;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ErrorProvider errorProvider7;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.ErrorProvider errorProvider8;
        private System.Windows.Forms.ErrorProvider errorProvider9;
        private System.Windows.Forms.ErrorProvider errorProvider10;
		private System.Windows.Forms.ToolStripMenuItem registrujToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem naruciToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem anamnezaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem hitnoToolStripMenuItem;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Prezime;
		private System.Windows.Forms.DataGridViewTextBoxColumn JMBG;
		private System.Windows.Forms.DataGridViewTextBoxColumn Grad;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem binarnoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox textBox10;
	}
}