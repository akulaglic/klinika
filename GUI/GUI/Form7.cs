﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;

namespace GUI
{
    public partial class Form7 : Form
    {
        private Klinika k17732 { get; set; }



        public Form7(Klinika k)
        {
            InitializeComponent();
            k17732 = k;

        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void Form7_Load(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if (p.raspored.postoji)
                {
                    ListViewItem l = new ListViewItem(p.knjizica.ime);
                    l.SubItems.Add(p.knjizica.prezime);
                    l.SubItems.Add(p.knjizica.jmbg);
                    l.SubItems.Add(p.ordinacija.ime_ordinacije);
                    l.SubItems.Add(Convert.ToString(p.knjizica.datum));
                    listView1.Items.Add(l);
                    
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    /*ListViewItem l = new ListViewItem(Form5.ime);
    l.SubItems.Add(Form5.prezime);
                    l.SubItems.Add(Form5.jmbg);
                    l.SubItems.Add(Form5.ordinacija);
                    l.SubItems.Add(Convert.ToString(Form5.datum));
                    listView1.Items.Add(l);*/
}
