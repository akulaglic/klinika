﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.Windows.Forms.DataVisualization.Charting;

namespace GUI
{
    public partial class Form8 : Form
    {
        private Klinika k17732 { get; set; }
        public Form8(Klinika k)
        {
            InitializeComponent();
            k17732 = k;
        }

        private void Form8_Paint(object sender, PaintEventArgs e)
        {
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void Form8_Load(object sender, EventArgs e)
        {
            Knjizica knj = new Knjizica();
            Pacijent pac = new Pacijent(knj);
            Racun r = new Racun(pac);
            foreach (Soba s in k17732.ordinacije)
            {
                
                if(s is Ordinacija)
                chart2.Series["Novac"].Points.AddXY((s as Ordinacija).ime_ordinacije,r.PostaviCijene((s as Ordinacija).ime_ordinacije));
            }
            int i = 0;

            chart1.Series.Clear();
            chart1.Palette = ChartColorPalette.Fire;
            chart1.BackColor = Color.LightYellow;
            chart1.ChartAreas[0].BackColor = Color.Transparent;

            Series series1 = new Series
            {
                Name = "series1",
                IsVisibleInLegend = true,
                Color = System.Drawing.Color.Green,
                ChartType = SeriesChartType.Pie
            };

            chart1.Series.Add(series1);

            foreach (Soba s in k17732.ordinacije)
            {
                if (s is Ordinacija)
                {

                   
                    series1.Points.Add((s as Ordinacija).broj_posjeta);
                    var p1 = series1.Points[i];
                    p1.AxisLabel = (s as Ordinacija).broj_posjeta.ToString();
                    p1.LegendText = (s as Ordinacija).ime_ordinacije;
                    i++;
                }
                
            }
            chart1.Invalidate();
            tabPage1.Controls.Add(chart1);

            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                chart3.Series["Posjeta"].Points.AddXY((p as NormalniPacijent).knjizica.ime+" "+ (p as NormalniPacijent).knjizica.prezime, p.posjeta);
            }
        }

        private void chart3_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1(k17732);
            this.Hide();
            f.ShowDialog();
            this.Close();
        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click_1(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
