﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;

namespace GUI
{
    public partial class Form4 : Form
    {
        private Klinika k17732 { get; set; }
        

        public Form4(Klinika k)
        {
            InitializeComponent();
            k17732 = k;
           
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            tabPage1_Click(sender, e);
            tabPage2_Click(sender, e);
            tabPage3_Click(sender, e);

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                if ((p as NormalniPacijent).knjizica.ime == Form3.ime &&
                        (p as NormalniPacijent).knjizica.prezime == Form3.prezime && (p as NormalniPacijent).knjizica.jmbg == Form3.jmbg 
                        ||((p as NormalniPacijent).knjizica.ime == Form6.ime &&
                        (p as NormalniPacijent).knjizica.prezime == Form6.prezime && (p as NormalniPacijent).knjizica.jmbg == Form6.jmbg)
                       )
                {
                    textBox4.Text = p.knjizica.ime;
                    textBox5.Text = p.knjizica.prezime;
                    textBox6.Text = p.knjizica.jmbg;
                    textBox7.Text = Convert.ToString(p.knjizica.datum_rodjenja);
                    textBox8.Text = p.knjizica.adresa;
                    textBox9.Text = p.knjizica.bracno_stanje;
                    textBox10.Text = Convert.ToString(p.knjizica.datum);
                    if (p.knjizica.spol == "musko") radioButton1.Checked = true;
                    else radioButton2.Checked = true;
                        //pictureBox1.Image = p.DajSliku();
                        slika1.PrikaziSliku(p.DajSliku());
                    }
                    
                    
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                    if ((p as NormalniPacijent).knjizica.ime == Form3.ime &&
                            (p as NormalniPacijent).knjizica.prezime == Form3.prezime && (p as NormalniPacijent).knjizica.jmbg == Form3.jmbg
                           || ((p as NormalniPacijent).knjizica.ime == Form6.ime &&
                        (p as NormalniPacijent).knjizica.prezime == Form6.prezime && (p as NormalniPacijent).knjizica.jmbg == Form6.jmbg)
                            )
                    {
                    richTextBox1.Text=p.inf_porodica ;
                    richTextBox2.Text=p.alergija ;
                    richTextBox3.Text=p.informacije_o_bolestima ;
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if (p is NormalniPacijent)
                    if ((p as NormalniPacijent).knjizica.ime == Form3.ime &&
                            (p as NormalniPacijent).knjizica.prezime == Form3.prezime && (p as NormalniPacijent).knjizica.jmbg == Form3.jmbg
                           || ((p as NormalniPacijent).knjizica.ime == Form6.ime &&
                        (p as NormalniPacijent).knjizica.prezime == Form6.prezime && (p as NormalniPacijent).knjizica.jmbg == Form6.jmbg)
                            )
                    {
                    richTextBox4.Text = p.zakljucak_doktora;
                    richTextBox5.Text = p.terapija;
                    
                }
            }
        }

       

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox4.Text = ""; textBox5.Text = ""; textBox6.Text = ""; textBox7.Text = "";
            textBox8.Text = ""; textBox9.Text = ""; textBox10.Text = "";
            richTextBox1.Text = ""; richTextBox2.Text = ""; richTextBox3.Text = ""; richTextBox4.Text = "";
            richTextBox5.Text = "";
            radioButton1.Checked = false; radioButton2.Checked = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
