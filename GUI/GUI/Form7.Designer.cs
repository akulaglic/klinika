﻿namespace GUI
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form7));
			this.terapijaBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.knjizicaBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.klinikaBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			((System.ComponentModel.ISupportInitialize)(this.terapijaBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.knjizicaBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.klinikaBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// terapijaBindingSource
			// 
			this.terapijaBindingSource.DataSource = typeof(AzrinaBiblioteka.Terapija);
			// 
			// knjizicaBindingSource
			// 
			this.knjizicaBindingSource.DataSource = typeof(AzrinaBiblioteka.Knjizica);
			// 
			// klinikaBindingSource
			// 
			this.klinikaBindingSource.DataSource = typeof(AzrinaBiblioteka.Klinika);
			// 
			// listView1
			// 
			this.listView1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.ForeColor = System.Drawing.Color.IndianRed;
			this.listView1.Location = new System.Drawing.Point(0, 0);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(769, 247);
			this.listView1.TabIndex = 2;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Ime";
			this.columnHeader1.Width = 130;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "Prezime";
			this.columnHeader7.Width = 163;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "JMBG";
			this.columnHeader8.Width = 197;
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "Ordinacija";
			this.columnHeader9.Width = 136;
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "Datum";
			this.columnHeader10.Width = 135;
			// 
			// Form7
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.ClientSize = new System.Drawing.Size(769, 247);
			this.Controls.Add(this.listView1);
			this.ForeColor = System.Drawing.Color.Red;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form7";
			this.Text = "Raspored";
			this.Load += new System.EventHandler(this.Form7_Load);
			((System.ComponentModel.ISupportInitialize)(this.terapijaBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.knjizicaBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.klinikaBindingSource)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource terapijaBindingSource;
        private System.Windows.Forms.BindingSource knjizicaBindingSource;
        private System.Windows.Forms.BindingSource klinikaBindingSource;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
    }
}