﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace GUI
{
    public partial class Form5 : Form
    {
        private Klinika k17732 { get; set; }
        
        public static Pacijent pac;

        public Form5(Klinika k)
        {
            InitializeComponent();
            k17732 = k;

          
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void Form5_Load(object sender, EventArgs e)
        {
            tabPage3_Click(sender, e);
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Knjizica knj = new Knjizica(textBox1.Text, textBox2.Text, textBox3.Text);
            knj.datum_rodjenja = dateTimePicker2.Value;
            knj.adresa = comboBox1.Text;
            knj.bracno_stanje = comboBox3.Text;
            if (radioButton1.Text == "Musko") knj.spol = "musko";
            else knj.spol = "zensko";
            knj.datum = dateTimePicker1.Value;
            NormalniPacijent p = new NormalniPacijent(knj);
            k17732.RegistrujPacijenta(p);
            p.PostaviSliku(slika1.DajSliku());
            if ((DateTime.Today.Month - slika1.DajDatum().Month) % 12 > 6)
            {
                statusStrip1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "Slika je starija od 6 mjeseci. Unesite drugu sliku!";
            }
            else
            {
                statusStrip1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "";
            }
            MessageBox.Show("Karton je kreiran.\nSifra pacijenta je "+p.knjizica.ime+p.knjizica.prezime+"\nID pacijenta je "+p.knjizica.ime+p.knjizica.prezime, "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {

            foreach (Pacijent p in k17732.pacijenti)
            {
				if (p is NormalniPacijent)
				{
					if (p.knjizica.ime == textBox5.Text && p.knjizica.prezime == textBox6.Text && p.knjizica.jmbg == textBox7.Text)
					{

						p.raspored.datum = dateTimePicker3.Value;
						
						foreach (Soba o in k17732.ordinacije)
							if (o is Ordinacija)
								if (comboBox2.Text == (o as Ordinacija).ime_ordinacije)
								{
									
									try
									{
										Form1.tehnicar.NaruciPacijenta(k17732, p, o as Ordinacija);
									}
									catch (ArgumentNullException i)
									{
										toolStripStatusLabel1.Text = i.Message;
										TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
									}
									
									pac = p;

									toolStripStatusLabel1.Text = "";
									MessageBox.Show("Uspješno narucen pacijent.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
									return;
								}
					}
				}
            }
            statusStrip1.ForeColor = Color.Red;
            toolStripStatusLabel1.Text = "Ne postoji pacijent sa tim podacima";

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1(k17732);
            this.Hide();
            f.ShowDialog();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
            Form7 f = new Form7(k17732);
            f.Show();
            
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

      
        private void button4_Click(object sender, EventArgs e)
        {
            foreach(Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                if((p as NormalniPacijent).knjizica.ime==textBox1.Text && 
                        (p as NormalniPacijent).knjizica.prezime==textBox2.Text &&
                        (p as NormalniPacijent).knjizica.jmbg==textBox3.Text)
                {
                    p.inf_porodica = richTextBox1.Text;
                    p.alergija = richTextBox2.Text;
                    p.informacije_o_bolestima = richTextBox3.Text;
                        toolStripStatusLabel1.Text = "";
                        MessageBox.Show("Uspješno uneseni podaci.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                }
            }
            statusStrip1.ForeColor = Color.Red;
            toolStripStatusLabel1.Text = "Ne postoji pacijent sa tim podacima";
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach(Pacijent p in k17732.pacijenti)
            {
                if (p is NormalniPacijent)
                    if ((p as NormalniPacijent).knjizica.ime == textBox1.Text &&
                            (p as NormalniPacijent).knjizica.prezime == textBox2.Text &&
                            (p as NormalniPacijent).knjizica.jmbg == textBox3.Text)
                    {
                    DialogResult dijalog= MessageBox.Show("Da li ste sigurni da zelite obrisati pacijenta?", "Pitanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dijalog == DialogResult.Yes)
                        {
                            k17732.broj_kartona--;
                            k17732.broj_pacijenata--;
                            k17732.ObrisiKarton(p.karton);
                            k17732.ObrisiPacijenta(p);

                            toolStripStatusLabel1.Text = "";
                            MessageBox.Show("Pacijent je obrisan.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else return;
                }
            }
            statusStrip1.ForeColor = Color.Red;
            toolStripStatusLabel1.Text = "Ne postoji pacijent sa tim podacima";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            foreach(Soba s in k17732.ordinacije)
            {
                if (s is Ordinacija) comboBox2.Items.Add((s as Ordinacija).ime_ordinacije);
            }
        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void slika1_Load(object sender, EventArgs e)
        {

        }
        private bool validIme(string ime, out string error)
        {
            if (ime == "")
            {
                error = "Ne smije biti prazno polje!";
                return false;
            }
            error = "";
            return true;
        }

        private bool validJMBG(string ime, out string error)
        {
            if (ime == "")
            {
                error = "Ne smije biti prazno polje!";
                return false;
            }
            if (ime.Length != 13)
            {
                error = "JMBG mora imati 13 cifara!";
                return false;

            }
            foreach (char c in ime)
            {
                if (c < 48 || c > 57)
                {
                    error = "JMBG mora sadrzavati brojeve!";
                    return false;
                }
            }
            error = "";
            return true;
        }


        private void textBox1_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(textBox1, "");
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox1.Text, out error))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                this.errorProvider1.SetError(textBox1, error);
            }
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            errorProvider2.SetError(textBox2, "");
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox2.Text, out error))
            {
                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                this.errorProvider2.SetError(textBox2, error);
            }
        }

        private void textBox3_Validated(object sender, EventArgs e)
        {
            errorProvider3.SetError(textBox3, "");
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validJMBG(textBox3.Text, out error))
            {
                e.Cancel = true;
                textBox3.Select(0, textBox3.Text.Length);
                this.errorProvider3.SetError(textBox3, error);
            }
        }

        private void textBox5_Validated(object sender, EventArgs e)
        {
            errorProvider4.SetError(textBox5, "");
        }

        private void textBox5_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox5.Text, out error))
            {
                e.Cancel = true;
                textBox5.Select(0, textBox5.Text.Length);
                this.errorProvider4.SetError(textBox5, error);
            }
        }

        private void textBox6_Validated(object sender, EventArgs e)
        {
            errorProvider5.SetError(textBox6, "");
        }

        private void textBox6_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox6.Text, out error))
            {
                e.Cancel = true;
                textBox6.Select(0, textBox6.Text.Length);
                this.errorProvider5.SetError(textBox6, error);
            }
        }

        private void textBox7_Validated(object sender, EventArgs e)
        {
            errorProvider6.SetError(textBox7, "");
        }

        private void textBox7_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validJMBG(textBox7.Text, out error))
            {
                e.Cancel = true;
                textBox7.Select(0, textBox7.Text.Length);
                this.errorProvider6.SetError(textBox7, error);
            }
        }

        private void slika1_Validated(object sender, EventArgs e)
        {
          
        }

        private void slika1_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker4_ValueChanged_1(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Knjizica k = new Knjizica(textBox4.Text, textBox8.Text, textBox9.Text);
            HitniPacijent pac = new HitniPacijent(k);
            pac.prva_pomoc = richTextBox4.Text;
            pac.uzrok = richTextBox5.Text;
            if (checkBox1.Checked)
            {
                pac.vrijeme_smrti = dateTimePicker4.Value;
                pac.datum_obdukcije = dateTimePicker5.Value;
                pac.prezivio = false;
				k17732.ObrisiPacijenta(pac);
				k17732.ObrisiKarton(pac.karton);
				k17732.broj_pacijenata--;
            }
            else {
                pac.prezivio = true;
            }
            k17732.pacijenti.Add(pac);
            MessageBox.Show("Uspješno unesen hitni pacijent.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) groupBox10.Visible = true;
            else groupBox10.Visible = false;
        }
        
        private void textBox4_Validated(object sender, EventArgs e)
        {
            errorProvider8.SetError(textBox4, "");
        }

        private void textBox4_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox4.Text, out error))
            {
                e.Cancel = true;
                textBox4.Select(0, textBox4.Text.Length);
                this.errorProvider8.SetError(textBox4, error);
            }
        }

        private void textBox8_Validated(object sender, EventArgs e)
        {
            errorProvider9.SetError(textBox8, "");
        }

        private void textBox8_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox8.Text, out error))
            {
                e.Cancel = true;
                textBox8.Select(0, textBox8.Text.Length);
                this.errorProvider9.SetError(textBox8, error);
            }
        }

        private void textBox9_Validated(object sender, EventArgs e)
        {
            errorProvider10.SetError(textBox9, "");
        }

        private void textBox9_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validJMBG(textBox9.Text, out error))
            {
                e.Cancel = true;
                textBox9.Select(0, textBox9.Text.Length);
                this.errorProvider10.SetError(textBox9, error);
            }
        }

        private void richTextBox4_Validated(object sender, EventArgs e)
        {

        }

        private void richTextBox4_Validating(object sender, CancelEventArgs e)
        {

        }

        private void richTextBox5_Validated(object sender, EventArgs e)
        {

        }

        private void richTextBox5_Validating(object sender, CancelEventArgs e)
        {

        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

		private void registrujToolStripMenuItem_Click(object sender, EventArgs e)
		{
			textBox1.Text = "";
			textBox2.Text = "";
			textBox3.Text = "";
			comboBox1.Text = "";
			comboBox3.Text = "";
			radioButton1.Checked = false;
			radioButton2.Checked = false;
			

		}

		private void naruciToolStripMenuItem_Click(object sender, EventArgs e)
		{
			textBox5.Text = "";
			textBox6.Text = "";
			textBox7.Text = "";
			comboBox2.Text = "";
		}

		private void anamnezaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.Text = "";
			richTextBox2.Text = "";
			richTextBox3.Text = "";
		}

		private void hitnoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			textBox4.Text = "";
			textBox8.Text = "";
			textBox9.Text = "";
			richTextBox4.Text = "";
			richTextBox5.Text = "";
		
		}

		private void button7_Click(object sender, EventArgs e)
		{
			try
			{
				if (Directory.Exists("Pacijenti"))
				{
					foreach (String s in Directory.GetFiles("Pacijenti"))
					{
						if (s.Contains(textBox10.Text))
						{
							IFormatter serijalizer = new BinaryFormatter();
							FileStream streamDatoteke = new FileStream(s, FileMode.Open);
							NormalniPacijent p = serijalizer.Deserialize(streamDatoteke) as NormalniPacijent;

							streamDatoteke.Close();
							treeView1.Nodes.Clear();
							int brojac = 0;
							foreach (Pacijent pac in k17732.pacijenti)
							{
								if (pac is NormalniPacijent)
								{
									treeView1.Nodes.Add((pac as NormalniPacijent).knjizica.ime + " " + (pac as NormalniPacijent).knjizica.prezime);
									treeView1.Nodes[brojac].Nodes.Add((pac as NormalniPacijent).knjizica.jmbg);
									treeView1.Nodes[brojac].Nodes.Add((pac as NormalniPacijent).knjizica.spol);
									treeView1.Nodes[brojac].Nodes.Add((pac as NormalniPacijent).knjizica.adresa);
									brojac++;
								}
							}


							return;
						}

					}
					MessageBox.Show("Pacijent ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);

				}
			}
			catch (Exception i)
			{
				TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());

			}
		}

		private void binarnoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				if (!File.Exists(textBox10.Text))
				{
					IFormatter serijalizer = new BinaryFormatter();
					FileStream stream = new FileStream("Pacijenti/" + textBox10.Text + ".txt", FileMode.Create);
					string spol;
					if (radioButton1.Checked) spol = "Musko";
					else spol = "Zensko";
					Knjizica knj = new Knjizica(textBox1.Text, textBox2.Text, textBox3.Text, spol, comboBox1.Text, comboBox3.Text);
					NormalniPacijent p = new NormalniPacijent(knj);
					serijalizer.Serialize(stream, p);
					/*Image s = slika1.DajSliku();
					
					if (s.ImageLocation == null)
					{
						s.ImageLocation = "../../InicijalnaProfilnaSlika.jpg";
					}*/

					MessageBox.Show("Pacijent je uspješno registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

					stream.Close();
				}
				else
				{
					MessageBox.Show("Tekstualna datoteka \"" + textBox10.Text + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}

			}
			catch (Exception i)
			{
				TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());

			}
		}

		public void TekstualnaDatoteka(string i)
		{
			try
			{
				if (!File.Exists(Form2.datoteka))
				{
					FileStream streamDatoteke = new FileStream("Izuzeci/" + Form2.datoteka + ".txt", FileMode.Create);
					StreamWriter streamPisac = new StreamWriter(streamDatoteke);
					streamPisac.WriteLine(i);

					//streamPisac.Flush();
					streamPisac.Close();
					streamDatoteke.Close();
				}
				else
				{
					MessageBox.Show("Tekstualna datoteka \"" + Form2.datoteka + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception izuzetak)
			{
				MessageBox.Show(izuzetak.Message, "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
