﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;

namespace GUI
{
    public partial class Form6 : Form
    {
        private Klinika k17732 { get; set; }
        public static string ime;
        public static string prezime;
        public static string jmbg;
        //private Pacijent p17732 { get; set; }

        public Form6(Klinika k)
        {
            InitializeComponent();
            k17732 = k;
            //p17732 = p;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            /*ListViewItem l = new ListViewItem(" agae");
            l.SubItems.Add("afa");
            l.SubItems.Add("afeaf");
            l.SubItems.Add("aeafga");
            listView1.Items.Add(l);*/
            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                if ((p as NormalniPacijent).raspored.postoji && (p as NormalniPacijent)==Form1.np)
                {
                    ListViewItem l = new ListViewItem(p.knjizica.ime);
                    l.SubItems.Add(p.knjizica.prezime);
                    l.SubItems.Add(p.knjizica.jmbg);
                    l.SubItems.Add(p.ordinacija.ime_ordinacije);
                    l.SubItems.Add(Convert.ToString(p.knjizica.datum));
                    listView1.Items.Add(l);
                }
            }

        }

        
        
        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                if ((p as NormalniPacijent).knjizica.ime == Form1.np.knjizica.ime && (p as NormalniPacijent).knjizica.prezime == Form1.np.knjizica.prezime 
                    && (p as NormalniPacijent).knjizica.jmbg == Form1.np.knjizica.jmbg)
                {
                    
                    ime = Form1.np.knjizica.ime;
                    prezime = Form1.np.knjizica.prezime;
                    jmbg = Form1.np.knjizica.jmbg;
                    Form4 f = new Form4(k17732);
                    f.Show();
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            /*foreach (Pacijent p in k17732.pacijenti)
            {
                if (p.knjizica.ime == p17732.knjizica.ime && p.knjizica.prezime == p17732.knjizica.prezime
                    && p.knjizica.jmbg == p17732.knjizica.jmbg)
                {

                    
                    Form7 f = new Form7(k17732);
                    f.Show();
                }
            }*/
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f2 = new Form1(k17732);
            this.Hide();
            f2.ShowDialog();
            this.Close();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int brojac = 0, i = 0;
            foreach (Soba s in k17732.ordinacije)
            {

                treeView1.Nodes.Add(s.ime_ordinacije);
                foreach (Doktor d in s.doktor)
                {
                    treeView1.Nodes[brojac].Nodes.Add(d.ime + " " + d.prezime);
                    foreach (EKarton n in d.kartoni)
                    {
                        treeView1.Nodes[brojac].Nodes[i].Nodes.Add(n.p.knjizica.ime + " " + n.p.knjizica.prezime);
                    }
                    i++;
                }
                brojac++;
            }
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            tabPage1_Click(sender, e);
            tabPage2_Click(sender, e);
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            int brojac = 0, i=0;
            foreach (Soba s in k17732.ordinacije)
            {

                treeView1.Nodes.Add(s.ime_ordinacije);
                foreach (Doktor d in s.doktor)
                {
                    treeView1.Nodes[brojac].Nodes.Add(d.ime+" "+d.prezime);
                    foreach(EKarton n in d.kartoni) {
                        treeView1.Nodes[brojac].Nodes[i].Nodes.Add(n.p.knjizica.ime+" "+ n.p.knjizica.prezime);
                    }
                    i++;
                }
                brojac++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(Pacijent p in k17732.pacijenti)
            {
                if (p is NormalniPacijent)
                    if (Form1.np.knjizica.ime == (p as NormalniPacijent).knjizica.ime && Form1.np.knjizica.prezime == (p as NormalniPacijent).knjizica.prezime
                               && Form1.np.knjizica.jmbg == (p as NormalniPacijent).knjizica.jmbg)
                    {
                        if(!radioButton1.Checked && !radioButton2.Checked) toolStripStatusLabel1.Text = "Oznacite sta zelite platiti!";
                        else {
                            if (radioButton1.Checked)
                            {
                                p.rate = true;
                                p.velicina_rate = Convert.ToInt32(numericUpDown1.Value);
                            }
                            else if (radioButton2.Checked) p.rate = false;

                            p.racun.da_li_je_platio = true;
                            p.racun.NaplataPacijenta(p, Form1.doktor.ordinacija);

                            richTextBox1.Text = p.racun.IspisiInterface(p, Form1.doktor);
                            toolStripStatusLabel1.Text = "";
                        }
                    }
                    
                    
            }
           

        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach(Pacijent p in k17732.pacijenti)
            {
                if (p is NormalniPacijent)
                    if (Form1.np.knjizica.ime == (p as NormalniPacijent).knjizica.ime && Form1.np.knjizica.prezime == (p as NormalniPacijent).knjizica.prezime
                           && Form1.np.knjizica.jmbg == (p as NormalniPacijent).knjizica.jmbg)
                        richTextBox1.Text = p.racun.IspisiInterface(p, Form1.doktor);
            }
            //richTextBox1.Text = Convert.ToString(Form1.np.dug);
            
        }

        
    }
}
