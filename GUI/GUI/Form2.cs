﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace GUI
{

	public partial class Form2 : Form
	{
		private Klinika k17732 { get; set; }
		private Doktor doktor { get; set; }
		private MedicinskiTehnicar tehnicar { get; set; }
		public static string datoteka { get; set; }
		List<Doktor> lista;
		List<MedicinskiTehnicar> sestre;

		public Form2(Klinika k1)
		{
			InitializeComponent();
			k17732 = k1;
			doktor = new Doktor();
			tehnicar = new MedicinskiTehnicar();
			lista = new List<AzrinaBiblioteka.Doktor>();
			sestre = new List<MedicinskiTehnicar>();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			MedicinskiTehnicar d = new MedicinskiTehnicar(Convert.ToInt32(numericUpDown2.Value), textBox6.Text, textBox5.Text);
			d.ordinacija.ime_ordinacije = comboBox2.Text;
			d.datumUnosa = dateTimePicker2.Value;
			k17732.DodajOsobu(d);
			if (textBox6.Text != "" || textBox5.Text != "" || Convert.ToInt32(numericUpDown2.Value) != 0)
				MessageBox.Show("Uspješno unesen medicinski tehnicar.\nSifra je " + d.ime + d.prezime + "\nID je " + d.ime + d.prezime, "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);

			k17732.broj_osoblja++;
		}

		private void button4_Click(object sender, EventArgs e)
		{
			foreach (MedicinskiTehnicar m in k17732.osoblje)
			{
				if (m.ime == textBox8.Text && m.prezime == textBox7.Text)
				{
					toolStripStatusLabel1.Text = "";
					DialogResult dijalog = MessageBox.Show("Da li zelite da obrisete medicinskog tehnicara?", "Pitanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (dijalog == DialogResult.Yes)
					{
						k17732.osoblje.Remove(m);

						MessageBox.Show("Uspješno obrisan doktor.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else return;
				}

			}
			statusStrip1.ForeColor = Color.Red;

			toolStripStatusLabel1.Text = "Ne postoji osoba!";


		}

		private void button5_Click(object sender, EventArgs e)
		{
			Form1 f2 = new Form1(k17732);
			this.Hide();
			f2.ShowDialog();
			this.Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Doktor d = new Doktor(Convert.ToInt32(numericUpDown1.Value), textBox1.Text, textBox2.Text);
			d.ordinacija.ime_ordinacije = comboBox1.Text;
			d.datumUnosa = dateTimePicker1.Value;
			foreach (Ordinacija o in k17732.ordinacije) if (o.ime_ordinacije == comboBox1.Text) o.doktor.Add(d);
			k17732.DodajOsobu(d);
			if (textBox1.Text != "" || textBox2.Text != "" || Convert.ToInt32(numericUpDown1.Value) != 0)
				MessageBox.Show("Uspješno unesen doktor.\nSifra je " + d.ime + d.prezime + "\nID je " + d.ime + d.prezime, "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);

			k17732.broj_osoblja++;

		}

		private void button2_Click(object sender, EventArgs e)
		{
			foreach (Doktor d in k17732.osoblje)
			{
				if (d.ime == textBox3.Text && d.prezime == textBox4.Text)
				{
					toolStripStatusLabel1.Text = "";
					DialogResult dijalog = MessageBox.Show("Da li ste sigurni da zelite obrisati doktora?", "Pitanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (dijalog == DialogResult.Yes)
					{
						k17732.osoblje.Remove(d);

						MessageBox.Show("Uspješno obrisan doktor.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else return;
				}
			}
			statusStrip1.ForeColor = Color.Red;

			toolStripStatusLabel1.Text = "Ne postoji osoba!";



		}

		private void button6_Click(object sender, EventArgs e)
		{

			Form1 f2 = new Form1(k17732);
			this.Hide();
			f2.ShowDialog();
			this.Close();
		}

		private void textBox6_TextChanged(object sender, EventArgs e)
		{

		}

		private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{



		}

		private void doktorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			textBox3.Text = "";
			textBox4.Text = "";
			textBox1.Text = "";
			textBox2.Text = "";
			numericUpDown1.Value = 0;
		}

		private void tehnicarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			textBox8.Text = "";
			textBox7.Text = "";
			textBox6.Text = "";
			textBox5.Text = "";
			numericUpDown2.Value = 0;
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Form1 f2 = new Form1(k17732);
			this.Hide();
			f2.ShowDialog();
			this.Close();
		}

		private void toolStripStatusLabel1_Click(object sender, EventArgs e)
		{

		}

		private void button7_Click(object sender, EventArgs e)
		{
			Ordinacija o = new Ordinacija(textBox9.Text);
			textBox9.Text = "";
			try
			{
				k17732.DodajOrdinaciju(o);
			}
			catch (NullReferenceException ex)
			{
				toolStripStatusLabel1.Text = ex.Message;

			}
			k17732.broj_ordinacija++;
			comboBox1.Items.Add(o.ime_ordinacije);
			comboBox2.Items.Add(o.ime_ordinacije);


		}

		private void textBox9_TextChanged(object sender, EventArgs e)
		{

		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}

		private void tabPage2_Click(object sender, EventArgs e)
		{
			foreach (Soba s in k17732.ordinacije) if (s is Ordinacija)
				{
					comboBox1.Items.Add((s as Ordinacija).ime_ordinacije);

				}
		}

		private void tabPage1_Click(object sender, EventArgs e)
		{
			foreach (Soba s in k17732.ordinacije) if (s is Ordinacija)
				{

					comboBox2.Items.Add((s as Ordinacija).ime_ordinacije);
				}
		}

		private void Form2_Load(object sender, EventArgs e)
		{
			tabPage1_Click(sender, e);
			tabPage2_Click(sender, e);
			tabPage4_Click(sender, e);
		}

		private void Form2_Paint(object sender, PaintEventArgs e)
		{



		}
		//VALIDACIJJA MORA IMATI VALIDATING I VALIDATED U MUNJI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		private void textBox1_Validated(object sender, EventArgs e)
		{
			errorProvider1.SetError(textBox1, "");
		}
		private bool validIme(string ime, out string error)
		{
			if (ime == "")
			{

				error = "Ne smije biti prazno polje!";
				return false;
			}
			error = "";
			return true;
		}

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validIme(textBox1.Text, out error))
			{
				e.Cancel = true;
				textBox1.Select(0, textBox1.Text.Length);
				this.errorProvider1.SetError(textBox1, error);
			}
		}

		private void textBox2_Validated(object sender, EventArgs e)
		{
			errorProvider2.SetError(textBox2, "");
		}

		private bool validPrezime(string prezime, out string error)
		{
			if (prezime == "")
			{

				error = "Ne smije biti porazno polje!";
				return false;
			}
			error = " ";
			return true;
		}

		private void textBox2_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validPrezime(textBox2.Text, out error))
			{
				e.Cancel = true;
				textBox2.Select(0, textBox2.Text.Length);
				this.errorProvider2.SetError(textBox2, error);
			}
		}

		// OBRISIIIIIIIIIIIIIIIIIII




		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}
		//za error provajder
		private void textBox3_Validated(object sender, EventArgs e)
		{
			errorProvider3.SetError(textBox3, "");
		}

		private void textBox3_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validIme(textBox3.Text, out error))
			{
				e.Cancel = true;
				textBox3.Select(0, textBox3.Text.Length);
				this.errorProvider3.SetError(textBox3, error);
			}
		}
		//dovdjee sve ovo
		private void textBox4_Validated(object sender, EventArgs e)
		{
			errorProvider4.SetError(textBox4, "");
		}

		private void textBox4_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validPrezime(textBox4.Text, out error))
			{
				e.Cancel = true;
				textBox4.Select(0, textBox4.Text.Length);
				this.errorProvider4.SetError(textBox4, error);
			}
		}

		private void textBox6_Validated(object sender, EventArgs e)
		{
			errorProvider5.SetError(textBox6, "");
		}



		private void textBox6_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validIme(textBox6.Text, out error))
			{
				e.Cancel = true;
				textBox6.Select(0, textBox6.Text.Length);
				this.errorProvider5.SetError(textBox6, error);
			}
		}
		private void textBox5_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validPrezime(textBox5.Text, out error))
			{
				e.Cancel = true;
				textBox5.Select(0, textBox5.Text.Length);
				this.errorProvider6.SetError(textBox5, error);
			}
		}

		private void textBox5_Validated(object sender, EventArgs e)
		{
			errorProvider6.SetError(textBox5, "");
		}

		private void textBox8_Validated(object sender, EventArgs e)
		{
			errorProvider7.SetError(textBox8, "");
		}

		private void textBox8_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validIme(textBox8.Text, out error))
			{
				e.Cancel = true;
				textBox8.Select(0, textBox8.Text.Length);
				this.errorProvider7.SetError(textBox8, error);
			}
		}

		private void textBox7_Validated(object sender, EventArgs e)
		{
			errorProvider8.SetError(textBox7, "");
		}

		private void textBox7_Validating(object sender, CancelEventArgs e)
		{
			string error;
			if (!validPrezime(textBox7.Text, out error))
			{
				e.Cancel = true;
				textBox7.Select(0, textBox7.Text.Length);
				this.errorProvider8.SetError(textBox7, error);
			}
		}

		private void textBox10_TextChanged(object sender, EventArgs e)
		{
			listBox1.Items.Clear();

			foreach (Soba o in k17732.ordinacije)
			{
				if (o is Ordinacija)
					if (((o as Ordinacija).ime_ordinacije).Contains(textBox10.Text))
					{
						listBox1.Items.Add((o as Ordinacija).ime_ordinacije);
					}
			}

		}

		private void tabPage4_Click(object sender, EventArgs e)
		{

		}

		private void tabPage4_Paint(object sender, PaintEventArgs e)
		{

		}

		private void binarnoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (groupBox1.Visible)
			{
				try
				{
					if (!File.Exists(textBox11.Text))
					{
						IFormatter serijalizer = new BinaryFormatter();
						FileStream stream = new FileStream("Osoblje/" + textBox11.Text + ".txt", FileMode.Create);

						Doktor d = new Doktor(Convert.ToInt32(numericUpDown1.Value), textBox1.Text, textBox2.Text);
						d.ordinacija.ime_ordinacije = comboBox1.Text;
						serijalizer.Serialize(stream, d);

						MessageBox.Show("Doktor je uspješno registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

						stream.Close();
					}

					else
					{
						MessageBox.Show("Tekstualna datoteka \"" + textBox11.Text + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}

				}
				catch (Exception i)
				{
					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				}

			}
			else if (groupBox3.Visible)
			{
				try
				{
					if (!File.Exists(textBox12.Text))
					{
						IFormatter serijalizer = new BinaryFormatter();
						FileStream stream = new FileStream("Osoblje/" + textBox12.Text + ".txt", FileMode.Create);
						MedicinskiTehnicar d = new MedicinskiTehnicar(Convert.ToInt32(numericUpDown2.Value), textBox6.Text, textBox5.Text);
						d.ordinacija.ime_ordinacije = comboBox2.Text;
						serijalizer.Serialize(stream, d);
						stream.Close();
						MessageBox.Show("Medicinski tehnicar je uspješno registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

					}
					else
					{
						MessageBox.Show("Tekstualna datoteka \"" + textBox12.Text + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}

				}
				catch (Exception i)
				{
					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				}
			}
		}

		private void button5_Click_1(object sender, EventArgs e)
		{
			treeView1.Nodes.Clear();
			if (groupBox1.Visible)
			{
				try
				{
					if (Directory.Exists("Osoblje"))
					{
						foreach (String s in Directory.GetFiles("Osoblje"))
						{
							if (s.Contains(textBox11.Text))
							{
								IFormatter serijalizer = new BinaryFormatter();
								FileStream streamDatoteke = new FileStream(s, FileMode.Open);
								Doktor d = serijalizer.Deserialize(streamDatoteke) as Doktor;

								
								lista.Add(d);
								

								foreach (Doktor doc in lista)
								{
									textBox11.Text =d.ordinacija.ime_ordinacije;
								}

									int brojac = 0;
								foreach (Soba sob in k17732.ordinacije)
								{

									treeView1.Nodes.Add(sob.ime_ordinacije);
									treeView1.Nodes[brojac].Nodes.Add("Doktori");

									foreach (Doktor doc in lista)
									{
										
										if (doc .ordinacija.ime_ordinacije == treeView1.Nodes[brojac].Text)
										{
											treeView1.Nodes[brojac].Nodes[0].Nodes.Add(doc.ime + " " + doc.prezime + " - " + doc.plata);

										}

									}
									brojac++;
								}

								streamDatoteke.Close();
								return;
							}

						}
						MessageBox.Show("Korisnik ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}
				}
				catch (Exception i)
				{
					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());

				}
			}
			else if (groupBox3.Visible)
			{
				try
				{
					if (Directory.Exists("Osoblje"))
					{
						foreach (String s in Directory.GetFiles("Osoblje"))
						{
							if (s.Contains(textBox12.Text))
							{
								IFormatter serijalizer = new BinaryFormatter();
								FileStream streamDatoteke = new FileStream(s, FileMode.Open);
								MedicinskiTehnicar d = serijalizer.Deserialize(streamDatoteke) as MedicinskiTehnicar;
								sestre.Add(d);
								
								int brojac = 0;
								foreach (Soba sob in k17732.ordinacije)
								{

									treeView1.Nodes.Add(sob.ime_ordinacije);

									treeView1.Nodes[brojac].Nodes.Add("Tehnicari");
									foreach (MedicinskiTehnicar doc in sestre)
									{
										
										if (doc.ordinacija.ime_ordinacije == treeView1.Nodes[brojac].Text)
											treeView1.Nodes[brojac].Nodes[0].Nodes.Add(doc.ime + " " + doc.prezime + " - " + doc.plata);

									}
									brojac++;
								}
								streamDatoteke.Close();


								return;
							}


						}

						MessageBox.Show("Korisnik ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}

					
				}
				catch (Exception i)
				{

					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				}

			}

		}

		private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
		{

		}

		private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
		{
			
				if (!File.Exists("Osoblje.xml"))
				{
					try
					{

						

						XmlSerializer xs = new XmlSerializer(typeof(List<Osoblje>));
						FileStream fs = new FileStream("Osoblje.xml", FileMode.Create);

						//Osoblje d = new Osoblje(Convert.ToInt32(numericUpDown1.Value), textBox1.Text, textBox2.Text);
						//d.ordinacija.ime_ordinacije = comboBox1.Text;

						xs.Serialize(fs, k17732.osoblje.ToList<Osoblje>());


						MessageBox.Show("Doktor je uspješno registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
						fs.Close();
					
				}
					catch (Exception ex )
					{
						TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + ex.GetType().ToString());
						MessageBox.Show("Doktor nije registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
						
					}

			}
			else
			{
			try
			{
				XmlSerializer xs = new XmlSerializer(typeof(List<Osoblje>));
				FileStream fs = new FileStream("studenti.xml", FileMode.Append);
				xs.Serialize(fs, k17732.osoblje.ToList<Osoblje>());
				fs.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Doktor nije registrovan.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
				
			}
		}

		

		}

		private void button6_Click_1(object sender, EventArgs e)
		{
			try
			{
				if (Directory.Exists("Izuzeci"))
				{
					foreach (String s in Directory.GetFiles("Izuzeci"))
					{
						if (s.Contains(textBox11.Text))
						{
							FileStream streamDatoteke = new FileStream(s, FileMode.Open);
							TextReader streamCitac = new StreamReader(streamDatoteke);

							List<string> korisnickiPodaci = new List<string>();


							richTextBox1.Text += streamCitac.ReadToEnd();

							streamCitac.Close();
							streamDatoteke.Close();

							return;
						}
					}
					
				}
				else
				{
					foreach (String s in Directory.GetFiles("Izuzeci"))
					{
						if (s.Contains(textBox11.Text))
						{
							FileStream streamDatoteke = new FileStream(s, FileMode.Append);
							TextReader streamCitac = new StreamReader(streamDatoteke);

							List<string> korisnickiPodaci = new List<string>();


							richTextBox1.Text += streamCitac.ReadToEnd();

							streamCitac.Close();
							streamDatoteke.Close();

							return;
						}
					}

				}
			}
			catch (Exception izuzetak)
			{
				MessageBox.Show(izuzetak.Message, "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}





		public void TekstualnaDatoteka(string i)
		{
			try
			{
				if (!File.Exists(textBox11.Text))
				{
					FileStream streamDatoteke = new FileStream("Izuzeci/" + textBox11.Text + ".txt", FileMode.Create);
					StreamWriter streamPisac = new StreamWriter(streamDatoteke);
					streamPisac.WriteLine(i );
					datoteka = textBox11.Text;
					//streamPisac.Flush();
					streamPisac.Close();
					streamDatoteke.Close();
				}
				else
				{
					MessageBox.Show("Tekstualna datoteka \"" + textBox11.Text + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception izuzetak)
			{
				MessageBox.Show(izuzetak.Message, "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void richTextBox1_TextChanged(object sender, EventArgs e)
		{
			 
		}

		private void button8_Click(object sender, EventArgs e)
		{
			treeView1.Nodes.Clear();
			if (groupBox1.Visible)
			{
				try
				{
					if (Directory.Exists("Osoblje"))
					{
						foreach (String s in Directory.GetFiles("Osoblje"))
						{
							if (s.Contains(textBox11.Text))
							{
								
								XmlSerializer xs = new XmlSerializer(typeof(List<Osoblje>));
								FileStream fs = new FileStream("korisnici.xml", FileMode.Open);
								k17732.osoblje = (List<Osoblje> )xs.Deserialize(fs);
								fs.Close();


								int brojac = 0;
								foreach (Soba sob in k17732.ordinacije)
								{

									treeView1.Nodes.Add(sob.ime_ordinacije);
									treeView1.Nodes[brojac].Nodes.Add("Doktori");

									foreach (Osoblje doc in k17732.osoblje)
									{
										if (doc is Doktor)
											if ((doc as Doktor).ordinacija.ime_ordinacije == treeView1.Nodes[brojac].Text)
											{
												treeView1.Nodes[brojac].Nodes[0].Nodes.Add((doc as Doktor).ime + " " + (doc as Doktor).prezime + " - " + (doc as Doktor).plata);

											}

									}
									brojac++;
								}


								return;
							}

						}
						MessageBox.Show("Korisnik ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}
				}
				catch (Exception i)
				{
					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());

				}
			}
			else if (groupBox3.Visible)
			{
				try
				{
					if (Directory.Exists("Osoblje"))
					{
						foreach (String s in Directory.GetFiles("Osoblje"))
						{
							if (s.Contains(textBox12.Text))
							{

								XmlSerializer xs = new XmlSerializer(typeof(List<Osoblje>));
								FileStream fs = new FileStream("korisnici.xml", FileMode.Open);
								k17732.osoblje= (List<Osoblje>)xs.Deserialize(fs) ;
								fs.Close();

								int brojac = 0;
								foreach (Soba sob in k17732.ordinacije)
								{

									treeView1.Nodes.Add(sob.ime_ordinacije);

									treeView1.Nodes[brojac].Nodes.Add("Tehnicari");
									foreach (Osoblje doc in k17732.osoblje)
									{
										if (doc is MedicinskiTehnicar)
											if ((doc as MedicinskiTehnicar).ordinacija.ime_ordinacije == treeView1.Nodes[brojac].Text)
												treeView1.Nodes[brojac].Nodes[0].Nodes.Add((doc as MedicinskiTehnicar).ime + " " + (doc as MedicinskiTehnicar).prezime + " - " + (doc as MedicinskiTehnicar).plata);

									}
									brojac++;
								}
								return;
							}


						}

						MessageBox.Show("Korisnik ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}


				}
				catch (Exception i)
				{

					TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				}

			}

		
	}

		private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

		}
	}
}
