﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzrinaBiblioteka;
using System.IO;

namespace GUI
{
    public partial class Form3 : Form
    {
        private Klinika k17732 { get; set; }
        private MedicinskiTehnicar m { get; set; }
        
        public static string ime;
        public static string prezime;
        public static string jmbg;
       
        public Form3(Klinika k)
        {
            InitializeComponent();
            k17732 = k;
            m = new MedicinskiTehnicar();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Form5 f = new Form5(k17732);
            //ListViewItem list = new ListViewItem(f.));

        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if(p is NormalniPacijent)
                if((p as NormalniPacijent).knjizica.ime==textBox1.Text && 
                        (p as NormalniPacijent).knjizica.prezime==textBox2.Text &&
                        (p as NormalniPacijent).knjizica.jmbg==textBox3.Text)
                {
                    ime = textBox1.Text;
                    prezime = textBox2.Text;
                    jmbg = textBox3.Text;
                    toolStripStatusLabel1.Text = "";
                    Form4 f = new Form4(k17732);
                    f.Show();
                    return;
                }
                
            }
            statusStrip1.ForeColor = Color.Red;
            toolStripStatusLabel1.Text = "Ne postoji pacijent pod tim podacima!";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(Pacijent p in k17732.pacijenti)
            {
                if(textBox1.Text==p.knjizica.ime && textBox2.Text==p.knjizica.prezime && textBox3.Text==p.knjizica.jmbg)
                {
                     foreach (Osoblje o in k17732.osoblje)
                     {
                        if (o is Doktor)
                            if (Form1.doktor.ime == (o as Doktor).ime && Form1.doktor.prezime == (o as Doktor).prezime)
                            {
                                (o as Doktor).DodatniPregled(p, (o as Doktor).ordinacija);
                                MessageBox.Show("Pacijent poslan na analizu.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
                     }

                }
            }
            
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Pacijent p in k17732.pacijenti)
                {
                    if (p.knjizica.ime == textBox1.Text && p.knjizica.prezime == textBox2.Text && p.knjizica.jmbg == textBox3.Text)
                    {
                        Raspored r = new Raspored(p);

                    }
                }
                Form7 f = new Form7(k17732);
                f.Show();
            }
            catch (InvalidCastException i)
            {
                statusStrip1.ForeColor = Color.Red;
				TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				statusStrip1.Text = "Pogresna konverzija Pacijenta";
                throw;
            }
            catch (InsufficientMemoryException i)
            {
                statusStrip1.ForeColor = Color.Red;
				TekstualnaDatoteka(DateTime.Now.ToString() + " Tip izuzetka je - " + i.GetType().ToString());
				statusStrip1.Text = "Nema memorije";
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
             
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f2 = new Form1(k17732);
            this.Hide();
            f2.ShowDialog();
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = ""; textBox3.Text = ""; textBox2.Text = "";
            richTextBox1.Text = ""; richTextBox2.Text = "";
            radioButton1.Checked = false; radioButton2.Checked=false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach (Pacijent p in k17732.pacijenti)
            {
                if (p.knjizica.ime == textBox1.Text && p.knjizica.prezime == textBox2.Text && p.knjizica.jmbg == textBox3.Text)
                {
                    p.zakljucak_doktora = richTextBox1.Text;
                    p.terapija = richTextBox2.Text;

                    foreach (Osoblje o in k17732.osoblje)
                    {
                        if (o is Doktor)
                        {
                            if (Form1.doktor.ime == (o as Doktor).ime && Form1.doktor.prezime == (o as Doktor).prezime)
                            {
                                (o as Doktor).kartoni.Remove(p.karton);
                                p.dug += p.racun.PostaviCijene((o as Doktor).ordinacija.ime_ordinacije);
                                 p.posjeta++;
								MessageBox.Show("Pregled je zavrsen.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
                        }
                    }
                    
                    
                }
            }

        }

        private bool validIme(string ime, out string error)
        {
            if (ime == "")
            {
                error = "Ne smije biti prazno polje!";
                return false;
            }
            error = "";
            return true;
        }


        private void textBox1_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(textBox1, "");
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox1.Text, out error))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                this.errorProvider1.SetError(textBox1, error);
            }
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            errorProvider2.SetError(textBox2, "");
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validIme(textBox2.Text, out error))
            {
                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                this.errorProvider2.SetError(textBox2, error);
            }
        }

        private void textBox3_Validated(object sender, EventArgs e)
        {
            errorProvider3.SetError(textBox3, "");
        }

        private bool validJMBG(string ime, out string error)
        {
            if (ime == "")
            {
                error = "Ne smije biti prazno polje!";
                return false;
            }
            if(ime.Length!=13)
            {
                error = "JMBG mora imati 13 cifara!";
                return false;

            }
            foreach(char c in ime)
            {
                if(c<48 ||  c>57)
                {
                    error = "JMBG mora sadrzavati brojeve!";
                    return false;
                }
            }
            error = "";
            return true;
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            string error;
            if (!validJMBG(textBox3.Text, out error))
            {
                e.Cancel = true;
                textBox3.Select(0, textBox3.Text.Length);
                this.errorProvider3.SetError(textBox3, error);
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            foreach (Soba s in k17732.ordinacije)
            {
                
                if (s is Ordinacija)
                    
                foreach (Pacijent p in (s as Ordinacija).zakazani_pacijenti)
                    {
                        
                        (s as Ordinacija).broj_posjeta++;
                        if (p is NormalniPacijent)
                            if ((p as NormalniPacijent).knjizica.ime == textBox1.Text &&
                                    (p as NormalniPacijent).knjizica.prezime == textBox2.Text &&
                                    (p as NormalniPacijent).knjizica.jmbg == textBox3.Text)
                            {
                                (p as NormalniPacijent).zakljucak_doktora = richTextBox1.Text;
                                (p as NormalniPacijent).terapija = richTextBox2.Text;

                                foreach (Osoblje o in k17732.osoblje)
                                {
                                    if (o is Doktor)
                                        if (Form1.doktor.ime == (o as Doktor).ime && Form1.doktor.prezime == (o as Doktor).prezime)
                                        {
                                            (o as Doktor).kartoni.Add(p.karton);
											statusStrip1.Text = "";
                                            MessageBox.Show("Uspješno uneseni podaci.", "Informacija", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            return;
                                        }

                                }

                            }

                    }

            }
            statusStrip1.ForeColor = Color.Red;
            toolStripStatusLabel1.Text = "Ne postoji pacijent pod tim podacima ili nije naručen!";
        }


	public void TekstualnaDatoteka(string i)
	{
		try
		{
			if (!File.Exists(Form2.datoteka))
			{
				FileStream streamDatoteke = new FileStream("Izuzeci/" +Form2.datoteka + ".txt", FileMode.Create);
				StreamWriter streamPisac = new StreamWriter(streamDatoteke);
				streamPisac.WriteLine(i);

				//streamPisac.Flush();
				streamPisac.Close();
				streamDatoteke.Close();
			}
			else
			{
				MessageBox.Show("Tekstualna datoteka \"" + Form2.datoteka + "\" već postoji.", "Obavještenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		catch (Exception izuzetak)
		{
			MessageBox.Show(izuzetak.Message, "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}
}

